/*
 * Reporte.java
 *
 * Created on 25 de mayo de 2007, 11:32 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

/**
 *
 * @author jdgomez
 */
public class Reporte {
    /**
     * Holds value of property idReporte.
     */
    private Integer idReporte;

    /**
     * Getter for property idReporte.
     * @return Value of property idReporte.
     */
    public Integer getIdReporte() {
        return this.idReporte;
    }

    /**
     * Setter for property idReporte.
     * @param idReporte New value of property idReporte.
     */
    public void setIdReporte(Integer idReporte) {
        this.idReporte = idReporte;
    }

    /**
     * Holds value of property nombre.
     */
    private String nombre;

    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Holds value of property descripcion.
     */
    private String descripcion;

    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public String getDescripcion() {
        return this.descripcion;
    }

    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString() {
        return nombre + " - " + descripcion;
    }

    /**
     * Holds value of property reporteDB.
     */
    private String reporteDB;

    /**
     * Getter for property reporteDB.
     * @return Value of property reporteDB.
     */
    public String getReporteDB() {
        return this.reporteDB;
    }

    /**
     * Setter for property reporteDB.
     * @param reporteDB New value of property reporteDB.
     */
    public void setReporteDB(String reporteDB) {
        this.reporteDB = reporteDB;
    }
    
}
