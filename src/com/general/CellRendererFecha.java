/*
 * CellRendererFecha.java
 *
 * Created on 6 de marzo de 2007, 05:05 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.general;

import java.awt.Color;
import java.awt.Component;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author jdgomez
 */
public class CellRendererFecha implements TableCellRenderer {
    
    /** Creates a new instance of CellRendererFecha */
    public CellRendererFecha() {
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JXDatePicker comp = new JXDatePicker();
       // comp.setFont(new java.awt.Font("Tahoma", 0, 10));
        comp.setDate((Date)value);
        GregorianCalendar gCal = new GregorianCalendar();
        gCal.setTime(new Date());
        System.out.println("table.getModel().getValueAt(row, 4-3): "+table.getModel().getValueAt(row, 4+1));
        int frecuencia=0;
        if((Integer)table.getModel().getValueAt(row, 4+1)==3)
            frecuencia=4;
        else
            frecuencia=(Integer)table.getModel().getValueAt(row, 4+1);
       // gCal.add(Calendar.DAY_OF_MONTH, -7*(Integer)table.getModel().getValueAt(row, 4+1));
         gCal.add(Calendar.DAY_OF_MONTH, -7*frecuencia);
        if (comp.getDate().before(gCal.getTime()) && table.getModel().getValueAt(row, 2) == null){
            comp.setBorder(BorderFactory.createLineBorder(Color.RED));
        }else
            comp.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        if(isSelected)
            comp.setBackground(table.getSelectionBackground());
        return comp;
    }
    
}
