/*
 * ReporteDAOImpl.java
 *
 * Created on 25 de mayo de 2007, 11:35 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.dao;

import java.util.List;
import org.springside.core.dao.HibernateEntityDao;
import pre.model.vo.Reporte;

/**
 *
 * @author jdgomez
 */
public class ReporteDAOImpl extends HibernateEntityDao<Reporte> {
    
    public List<Reporte> getAllReportes() {
        return getAll();
    }
    
}
