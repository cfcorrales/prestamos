/*
 * Pago.java
 *
 * Created on 23 de febrero de 2007, 10:01 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

import java.util.Date;

/**
 *
 * @author jdgomez
 */
public class Pago {
    /**
     * Holds value of property idPago.
     */
    private Long idPago;
    private Date fechaIngresoPago;
    private String frecuenciaLetras;

    public String getFrecuenciaLetras() {
        if(null!=this.frecuenciaCobro) {
            if(this.frecuenciaCobro==1)//semanal
                return "Semanal";
            if(this.frecuenciaCobro==2)//semanal
                return "Quincenal";
            if(this.frecuenciaCobro==3)//semanal
                return "Mensual";
            if(this.frecuenciaCobro==4)//semanal
                return "Diaria";

        }

        return frecuenciaLetras;
    }

    public void setFrecuenciaLetras(String frecuenciaLetras) {
        this.frecuenciaLetras = frecuenciaLetras;
    }



    public Date getFechaIngresoPago() {
        return fechaIngresoPago;
    }

    public void setFechaIngresoPago(Date fechaIngresoPago) {
        this.fechaIngresoPago = fechaIngresoPago;
    }

    
    /**
     * Getter for property idPago.
     * @return Value of property idPago.
     */
    public Long getIdPago() {
        return this.idPago;
    }

    /**
     * Setter for property idPago.
     * @param idPago New value of property idPago.
     */
    public void setIdPago(Long idPago) {
        this.idPago = idPago;
    }

    /**
     * Holds value of property numPago.
     */
    private Integer numPago;

    /**
     * Getter for property numPago.
     * @return Value of property numPago.
     */
    public Integer getNumPago() {
        return this.numPago;
    }

    /**
     * Setter for property numPago.
     * @param numPago New value of property numPago.
     */
    public void setNumPago(Integer numPago) {
        this.numPago = numPago;
    }

    /**
     * Holds value of property fechaPago.
     */
    private Date fechaPago;

    /**
     * Getter for property fechaPago.
     * @return Value of property fechaPago.
     */
    public Date getFechaPago() {
        return this.fechaPago;
    }

    /**
     * Setter for property fechaPago.
     * @param fechaPago New value of property fechaPago.
     */
    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * Holds value of property valorAPagar.
     */
    private Long valorAPagar;

    public Long getValorAPagar() {
        return valorAPagar;
    }

    public void setValorAPagar(Long valorAPagar) {
        this.valorAPagar = valorAPagar;
    }

    
    /**
     * Holds value of property valorPagado.
     */
    private Long valorPagado;

    public Long getValorPagado() {
        return valorPagado;
    }

    public void setValorPagado(Long valorPagado) {
        this.valorPagado = valorPagado;
    }

   

    /**
     * Holds value of property estado.
     */
    private Integer estado;

    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public Integer getEstado() {
        return this.estado;
    }

    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    /**
     * Holds value of property frecuenciaCobro.
     */
    private Integer frecuenciaCobro;

    /**
     * Getter for property frecuenciaCobro.
     * @return Value of property frecuenciaCobro.
     */
    public Integer getFrecuenciaCobro() {
        return this.frecuenciaCobro;
    }

    /**
     * Setter for property frecuenciaCobro.
     * @param frecuenciaCobro New value of property frecuenciaCobro.
     */
    public void setFrecuenciaCobro(Integer frecuenciaCobro) {
        this.frecuenciaCobro = frecuenciaCobro;
    }

    
    
}
