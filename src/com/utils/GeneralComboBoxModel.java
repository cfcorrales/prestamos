package com.utils;

import java.util.List;

import javax.swing.DefaultComboBoxModel;


public class GeneralComboBoxModel extends DefaultComboBoxModel {
    private List datos;
    Object selectedObject;


    public GeneralComboBoxModel(List datos) {
        this.datos = datos;
    }
    
    public void setSelectedItem(Object anObject) {
        if ((selectedObject != null && !selectedObject.equals( anObject )) ||
            selectedObject == null && anObject != null) {
            selectedObject = anObject;
            fireContentsChanged(this, -1, -1);
        }
    }

    public Object getSelectedItem() {
        return selectedObject;
    }

    public int getSize() {
        return datos.size() + 1;
    }

   public Object getElementAt(int index) {
        if (index == 0)
            return "";
        if ( index >= 1 && index < datos.size() + 1)
            return datos.get(index - 1);
        else
            return null;
    }
    
}
