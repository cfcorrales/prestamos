/*
 * Propiedad.java
 *
 * Created on 23 de febrero de 2007, 09:45 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

import java.util.Date;

/**
 *
 * @author jdgomez
 */
public class Propiedad {

    /**
     * Holds value of property idPropiedad.
     */
    private Long idPropiedad;

    /**
     * Getter for property idPropiedad.
     * @return Value of property idPropiedad.
     */
    public Long getIdPropiedad() {
        return this.idPropiedad;
    }

    /**
     * Setter for property idPropiedad.
     * @param idPropiedad New value of property idPropiedad.
     */
    public void setIdPropiedad(Long idPropiedad) {
        this.idPropiedad = idPropiedad;
    }

    /**
     * Holds value of property propiedad.
     */
    private String propiedad;

    /**
     * Getter for property tipoPropiedad.
     * @return Value of property tipoPropiedad.
     */
    public String getPropiedad() {
        return this.propiedad;
    }

    /**
     * Setter for property tipoPropiedad.
     * @param tipoPropiedad New value of property tipoPropiedad.
     */
    public void setPropiedad(String propiedad) {
        this.propiedad = propiedad;
    }

    /**
     * Holds value of property descripcion.
     */
    private String descripcion;

    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public String getDescripcion() {
        return this.descripcion;
    }

    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Holds value of property valorPropiedad.
     */
    private Long valorPropiedad;

    /**
     * Getter for property valorPropiedad.
     * @return Value of property valorPropiedad.
     */
    public Long getValorPropiedad() {
        return this.valorPropiedad;
    }

    /**
     * Setter for property valorPropiedad.
     * @param valorPropiedad New value of property valorPropiedad.
     */
    public void setValorPropiedad(Long valorPropiedad) {
        this.valorPropiedad = valorPropiedad;
    }

    /**
     * Holds value of property datosAdicionales.
     */
    private String datosAdicionales;

    /**
     * Holds value of property estado.
     */
    private Boolean estado;

    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public Boolean getEstado() {
        return this.estado;
    }

    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    /**
     * Holds value of property fechaCreacion.
     */
    private Date fechaCreacion;

    /**
     * Getter for property fechaCreacion.
     * @return Value of property fechaCreacion.
     */
    public Date getFechaCreacion() {
        return new Date();
    }

    /**
     * Setter for property fechaCreacion.
     * @param fechaCreacion New value of property fechaCreacion.
     */
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    public String toString() {
        return propiedad;
    }
    
}
