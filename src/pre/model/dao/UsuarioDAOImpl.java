/*
 * UsuarioDAOImpl.java
 *
 * Created on 19 de mayo de 2007, 11:51 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.dao;

import org.springside.core.dao.HibernateEntityDao;
import pre.model.vo.Usuario;

/**
 *
 * @author jdgomez
 */
public class UsuarioDAOImpl extends HibernateEntityDao<Usuario> {
    
    public Usuario findUsuarioByLogin(String login) {
        return findUniqueBy("login", login);
        
    }
    
    public void saveUsuario(Usuario usuario) {
        save(usuario);
    }
}
