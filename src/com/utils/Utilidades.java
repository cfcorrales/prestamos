/*
 * Utilidades.java
 *
 * Created on 3 de marzo de 2004, 10:31 PM
 */

package com.utils;
import java.util.*;
import javax.swing.*;
import javax.swing.text.JTextComponent;
import org.jdesktop.swingx.JXDatePicker;


/**
 *
 * @author  Admin
 */
    public abstract class Utilidades {
    
    public static void limpiar(JComponent p) {
        if (p instanceof JComboBox)
            ((JComboBox)p).setSelectedIndex(-1);
        else if (p instanceof JXDatePicker)
            ((JXDatePicker)p).setDate(null);
        else if(p instanceof JTextComponent)
            ((javax.swing.text.JTextComponent)p).setText("");
        else if(p instanceof JCheckBox)
            ((JCheckBox)p).setSelected(false);
        else {
            if (p.getComponentCount() > 0) {
                for (int i=0; i < p.getComponentCount(); i++)
                    limpiar((JComponent)p.getComponent(i));
            }
        }
    }
    
    public static void estado(JComponent p, boolean estado) {
        if (p instanceof JComboBox)
            ((JComboBox)p).setEnabled(estado);
        else if (p instanceof JXDatePicker)
            ((JXDatePicker)p).setEnabled(estado);
        else if(p instanceof JTextComponent)
            ((javax.swing.text.JTextComponent)p).setEnabled(estado);
        else if(p instanceof JCheckBox)
            ((JCheckBox)p).setEnabled(estado);
        else {
            if (p.getComponentCount() > 0) {
                for (int i=0; i < p.getComponentCount(); i++)
                    estado((JComponent)p.getComponent(i), estado);
            }
        }
    }
    
    public static String validar(String dato, String mess) throws MyExceptionVal {
        if(dato.equals(""))
            throw new MyExceptionVal("Debe Ingresar " + mess);
        return dato;
    }
    
    public static Date validarFecha(Date dato, String mess) throws MyExceptionVal {
        if (dato == null)
            throw new MyExceptionVal("Debe Ingresar " + mess);
        return dato;
    }
    
        /**
     * Retorna el texto con la primera letra en mayuscula y el resto las deja igual
     */
    public static String capitalize(String texto) {
       try{
        StringBuffer result = new StringBuffer();
        result.append(Character.toUpperCase(texto.charAt(0)));
        result.append(texto.substring(1));
        return result.toString();
       }catch(Exception e){
           System.out.println("error: "+e.getMessage());
           return null;
       }
       
    }
    
    /**
     * Retorna el texto con la primera letra en mayuscula y el resto en minusculas
     */
    public static String toInitCap(String texto) {
        StringBuffer result = new StringBuffer();
        result.append(Character.toUpperCase(texto.charAt(0)));
        result.append(texto.substring(1).toLowerCase());
        return result.toString();
    }
    
}
