/*
 * Sede.java
 *
 * Created on 23 de febrero de 2007, 08:40 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

import java.util.Date;

/**
 *
 * @author jdgomez
 */
public class Sede {
    
    /**
     * Holds value of property idSede.
     */
    private Integer idSede;

    /**
     * Getter for property idSede.
     * @return Value of property idSede.
     */
    public Integer getIdSede() {
        return this.idSede;
    }

    /**
     * Setter for property idSede.
     * @param idSede New value of property idSede.
     */
    public void setIdSede(Integer idSede) {
        this.idSede = idSede;
    }

    /**
     * Holds value of property nombreSede.
     */
    private String nombreSede;

    /**
     * Getter for property nombreSede.
     * @return Value of property nombreSede.
     */
    public String getNombreSede() {
        return this.nombreSede;
    }

    /**
     * Setter for property nombreSede.
     * @param nombreSede New value of property nombreSede.
     */
    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    /**
     * Holds value of property direccionSede.
     */
    private String direccionSede;

    /**
     * Getter for property direccionSede.
     * @return Value of property direccionSede.
     */
    public String getDireccionSede() {
        return this.direccionSede;
    }

    /**
     * Setter for property direccionSede.
     * @param direccionSede New value of property direccionSede.
     */
    public void setDireccionSede(String direccionSede) {
        this.direccionSede = direccionSede;
    }

    /**
     * Holds value of property telefonoSede.
     */
    private String telefonoSede;

    /**
     * Getter for property telefonoSede.
     * @return Value of property telefonoSede.
     */
    public String getTelefonoSede() {
        return this.telefonoSede;
    }

    /**
     * Setter for property telefonoSede.
     * @param telefonoSede New value of property telefonoSede.
     */
    public void setTelefonoSede(String telefonoSede) {
        this.telefonoSede = telefonoSede;
    }

    /**
     * Holds value of property encargadoSede.
     */
    private String encargadoSede;

    /**
     * Getter for property encargadoSede.
     * @return Value of property encargadoSede.
     */
    public String getEncargadoSede() {
        return this.encargadoSede;
    }

    /**
     * Setter for property encargadoSede.
     * @param encargadoSede New value of property encargadoSede.
     */
    public void setEncargadoSede(String encargadoSede) {
        this.encargadoSede = encargadoSede;
    }

    /**
     * Holds value of property estadoSede.
     */
    private Boolean estadoSede;

    /**
     * Getter for property estadoSede.
     * @return Value of property estadoSede.
     */
    public Boolean getEstadoSede() {
        return this.estadoSede;
    }

    /**
     * Setter for property estadoSede.
     * @param estadoSede New value of property estadoSede.
     */
    public void setEstadoSede(Boolean estadoSede) {
        this.estadoSede = estadoSede;
    }

    /**
     * Holds value of property fechaCreacion.
     */
    private Date fechaCreacion;

    /**
     * Getter for property fechaCreacion.
     * @return Value of property fechaCreacion.
     */
    public Date getFechaCreacion() {
        return new Date();
    }

    /**
     * Setter for property fechaCreacion.
     * @param fechaCreacion New value of property fechaCreacion.
     */
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    public String toString() {
        return nombreSede;
    }
    
}
