/*
 * Main.java
 *
 * Created on 23 de febrero de 2007, 08:24 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package prestamos;

import java.util.Date;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pre.cliente.FrmGeneral;
import pre.cliente.FrmSelSedes;
import pre.model.vo.Credito;
import pre.model.vo.Persona;
import pre.services.PrestamoServiceImpl;
import pre.model.vo.Sede;

/**
 *
 * @author jdgomez
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmSelSedes().setVisible(true);
            }
        });
    }
    
}
