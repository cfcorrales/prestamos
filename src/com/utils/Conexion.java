/*
 * Conexion.java
 *
 * Created on 19 de enero de 2006, 07:22 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Jes�s David
 */
public class Conexion {
    private Connection conn = null;
    
    /** Creates a new instance of Conexion */
    public Conexion() {
    }
    
    public void registrarDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (InstantiationException e) {
            System.err.println(e.getMessage());
        } catch (IllegalAccessException e) {
            System.err.println(e.getMessage());                        
        }
    }
    
    public void iniciarConexion() {
        try {
            String user = ResourceHelper.getProperty("prop/Propiedades.properties", "user");
            String password = ResourceHelper.getProperty("prop/Propiedades.properties", "passwd");
            String servidor = ResourceHelper.getProperty("prop/Propiedades.properties", "servidor");
            String basedatos = ResourceHelper.getProperty("prop/Propiedades.properties", "basedatos");
            conn = DriverManager.getConnection("jdbc:mysql://" + servidor + "/" + basedatos, user, password);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public void cerrarConexion() {
        try {
            if (conn != null) {
                conn.close();
            }                
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }        
    }
    
    public Connection getConexion() {
        if (conn == null)
            iniciarConexion();        
        return conn;
    }

    
}
