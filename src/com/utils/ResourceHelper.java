/*
 * ResourceHelper.java
 *
 * Created on 7 de junio de 2004, 19:00
 */

package com.utils;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author  jdgomez
 */
public class ResourceHelper {
    
    /** Creates a new instance of ResourceHelper */
    public ResourceHelper() {
    }
    
    public static URL getURL(String file) {
        URL url = null;
        ClassLoader loader = ResourceHelper.class.getClassLoader();
        url = loader.getResource(file);
        if(url==null)
            url = ClassLoader.getSystemResource(file);
        if(url==null) {
            File arch = new File(file);
            try {
                if(arch.exists()&&arch.isFile()) {
                    url = arch.toURI().toURL();
                }
            }catch(MalformedURLException e) {}
        }
        return url;
    }
    
    public static ImageIcon getImagen(String resource) {
        URL imgURL = getURL(resource);
        if(imgURL!=null)
            return new ImageIcon(imgURL);
        else
            return null;
    }
    
    public static Properties loadProperties(String resource) {
        Properties prop = new Properties();
        InputStream is = null;
        try {
            is = getURL(resource).openStream();
            prop.load(is);
            is.close();
        }catch(IOException e) {}
        return prop;
    }
    
    public static String getProperty(String resource, String key) {
        String prop = null;
        try {
            prop = loadProperties(resource).getProperty(key);
            loadProperties(resource).setProperty(key, "0");
        }catch(Exception e) {}
        return prop;
    }
    
}
