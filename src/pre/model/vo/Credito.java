/*
 * Credito.java
 *
 * Created on 23 de febrero de 2007, 09:50 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author jdgomez
 */
public class Credito {
    /**
     * Holds value of property idCredito.
     */
    private Long idCredito;
   
    private Integer ruta;

    public Integer getRuta() {
        return ruta;
    }

    public void setRuta(Integer ruta) {
        this.ruta = ruta;
    }
    
    /**
     * Getter for property idCredito.
     * @return Value of property idCredito.
     */
    public Long getIdCredito() {
        return this.idCredito;
    }
    
    /**
     * Setter for property idCredito.
     * @param idCredito New value of property idCredito.
     */
    public void setIdCredito(Long idCredito) {
        this.idCredito = idCredito;
    }
    
    /**
     * Holds value of property idCobrador.
     */
    private Integer idCobrador;
    
    /**
     * Getter for property idCobrador.
     * @return Value of property idCobrador.
     */
    public Integer getIdCobrador() {
        return this.idCobrador;
    }
    
    /**
     * Setter for property idCobrador.
     * @param idCobrador New value of property idCobrador.
     */
    public void setIdCobrador(Integer idCobrador) {
        this.idCobrador = idCobrador;
    }
    
    /**
     * Holds value of property valorPrestado.
     */
    private Long valorPrestado;

    public Long getValorPrestado() {
        return valorPrestado;
    }

    public void setValorPrestado(Long valorPrestado) {
        this.valorPrestado = valorPrestado;
    }
    
    
    /**
     * Holds value of property valorTotalPagar.
     */
    private Long valorTotalPagar;

    public Long getValorTotalPagar() {
        return valorTotalPagar;
    }

    public void setValorTotalPagar(Long valorTotalPagar) {
        this.valorTotalPagar = valorTotalPagar;
    }
    
    
    
    /**
     * Holds value of property fechaInicio.
     */
    private Date fechaInicio;
    
    /**
     * Getter for property fechaInicio.
     * @return Value of property fechaInicio.
     */
    public Date getFechaInicio() {
        return this.fechaInicio;
    }
    
    /**
     * Setter for property fechaInicio.
     * @param fechaInicio New value of property fechaInicio.
     */
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    
    /**
     * Holds value of property numCuotas.
     */
    private Integer numCuotas;
    
    /**
     * Getter for property numCuotas.
     * @return Value of property numCuotas.
     */
    public Integer getNumCuotas() {
        return this.numCuotas;
    }
    
    /**
     * Setter for property numCuotas.
     * @param numCuotas New value of property numCuotas.
     */
    public void setNumCuotas(Integer numCuotas) {
        this.numCuotas = numCuotas;
    }
    
    /**
     * Holds value of property fechaCreacion.
     */
    private Date fechaCreacion;
    
    /**
     * Getter for property fechaCreacion.
     * @return Value of property fechaCreacion.
     */
    public Date getFechaCreacion() {
        return new Date();
    }
    
    /**
     * Setter for property fechaCreacion.
     * @param fechaCreacion New value of property fechaCreacion.
     */
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    /**
     * Holds value of property deudaActual.
     */
    private Long deudaActual;

    public Long getDeudaActual() {
        return deudaActual;
    }

    public void setDeudaActual(Long deudaActual) {
        this.deudaActual = deudaActual;
    }
    
   
    
    /**
     * Holds value of property valorCuota.
     */
    private Long valorCuota;

    public Long getValorCuota() {
        return valorCuota;
    }

    public void setValorCuota(Long valorCuota) {
        this.valorCuota = valorCuota;
    }
    
   
    /**
     * Holds value of property cuotaAtrazo.
     */
    private Float cuotaAtrazo;
    
    /**
     * Getter for property cuotaAtrazo.
     * @return Value of property cuotaAtrazo.
     */
    public Float getCuotaAtrazo() {
        return this.cuotaAtrazo;
    }
    
    /**
     * Setter for property cuotaAtrazo.
     * @param cuotaAtrazo New value of property cuotaAtrazo.
     */
    public void setCuotaAtrazo(Float cuotaAtrazo) {
        this.cuotaAtrazo = cuotaAtrazo;
    }
    
    /**
     * Holds value of property frecuenciaCobro.
     */
    private Integer frecuenciaCobro;
    
    /**
     * Getter for property frecuenciaCobro.
     * @return Value of property frecuenciaCobro.
     */
    public Integer getFrecuenciaCobro() {
        return this.frecuenciaCobro;
    }
    
    /**
     * Setter for property frecuenciaCobro.
     * @param frecuenciaCobro New value of property frecuenciaCobro.
     */
    public void setFrecuenciaCobro(Integer frecuenciaCobro) {
        this.frecuenciaCobro = frecuenciaCobro;
    }
    
    /**
     * Holds value of property estado.
     */
    private Integer estado;
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public Integer getEstado() {
        return this.estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(Integer estado) {
        this.estado = estado;
    }
    
    /**
     * Holds value of property numeroCredito.
     */
    private String numeroCredito;
    
    /**
     * Getter for property numeroCredito.
     * @return Value of property numeroCredito.
     */
    public String getNumeroCredito() {
        return this.numeroCredito;
    }
    
    /**
     * Setter for property numeroCredito.
     * @param numeroCredito New value of property numeroCredito.
     */
    public void setNumeroCredito(String numeroCredito) {
        this.numeroCredito = numeroCredito;
    }

    /**
     * Holds value of property pagos.
     */
    private Set<Pago> pagos = new HashSet();

    /**
     * Getter for property pagos.
     * @return Value of property pagos.
     */
    public java.util.Set<pre.model.vo.Pago> getPagos() {
        return this.pagos;
    }

    /**
     * Setter for property pagos.
     * @param pagos New value of property pagos.
     */
    public void setPagos(java.util.Set<pre.model.vo.Pago> pagos) {
        this.pagos = pagos;
    }

    /**
     * Holds value of property idPersona.
     */
    private Long idPersona;

    /**
     * Getter for property idPersona.
     * @return Value of property idPersona.
     */
    public Long getIdPersona() {
        return this.idPersona;
    }

    /**
     * Setter for property idPersona.
     * @param idPersona New value of property idPersona.
     */
    public void setIdPersona(Long idPersona) {
        this.idPersona = idPersona;
    }
    
    public String toString() {
        String estadoC="";
        if(estado!=null){
            if(estado==1)
                estadoC="Activo";
            else if(estado==2)
                estadoC="Por Activar";
            else if(estado==0)
                estadoC="Terminado";
        }
        
        return numeroCredito + " - " + fechaInicio.toString()+" - "+estadoC;
    }

    /**
     * Holds value of property codeudores.
     */
    private Set<Codeudor> codeudores  = new HashSet<Codeudor>();

    /**
     * Getter for property codeudores.
     * @return Value of property codeudores.
     */
    public Set<Codeudor> getCodeudores() {
        return this.codeudores;
    }

    /**
     * Setter for property codeudores.
     * @param codeudores New value of property codeudores.
     */
    public void setCodeudores(Set<Codeudor> codeudores) {
        this.codeudores = codeudores;
    }
    
    
}
