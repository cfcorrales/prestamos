/*
 * PrestamoServiceImpl.java
 *
 * Created on 23 de febrero de 2007, 08:56 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import pre.model.dao.CobradorDAOImpl;
import pre.model.dao.CreditoDAOImpl;
import pre.model.dao.PersonaDAOImpl;
import pre.model.dao.ReporteDAOImpl;
import pre.model.dao.SedeDAOImpl;
import pre.model.dao.UsuarioDAOImpl;
import pre.model.vo.Cobrador;
import pre.model.vo.Credito;
import pre.model.vo.Persona;
import pre.model.vo.Reporte;
import pre.model.vo.Sede;
import pre.model.vo.Usuario;

/**
 *
 * @author jdgomez
 */
public class PrestamoServiceImpl {
    
    public void saveSede(Sede sede) {
        sedeDAO.saveSede(sede);
    }
    
    public void removeCredito(Credito credito) {
        creditoDAO.removeCredito(credito);
    }
    
    public Usuario findUsuarioByLogin(String login) {
        return usuarioDAO.findUsuarioByLogin(login);
    }
    
    public List<Reporte> getAllReportes() {
        return reporteDAO.getAllReportes();
    }
    
    public void activarCreditos() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        GregorianCalendar gCal = new GregorianCalendar();
        gCal.add(Calendar.DAY_OF_MONTH, -6);
        Date fecha = gCal.getTime();
        String hql = "from Credito where estado = 2 and fechaInicio <= ?";
        List<Credito> creditos = creditoDAO.findCredito(hql, new Object[]{sdf.format(fecha)});
        for(int i = 0; i < creditos.size(); i++) {
            activarCredito(creditos.get(i));
        }
    }
    
    public void activarCredito(Credito credito) {
        credito.setEstado(1);
        crearPagos(credito);
        saveCredito(credito);
    }
    
    public void saveUsuario(Usuario usuario) {
        usuarioDAO.saveUsuario(usuario);
    }
    
    public void saveCobrador(Cobrador cobrador) {
        cobradorDAO.saveCobrador(cobrador);
    }
    
    public List<Sede> getSedes() {
        return sedeDAO.getSedes();
    }
    
    public List<Cobrador> getCobradoresBySede(Integer idSede) {
        return cobradorDAO.findCobradorBySede(idSede);
    }
    
    public void savePersona(Persona persona) {
        personaDAO.savePersona(persona);
    }
    
    public void saveCredito(Credito credito) {
        creditoDAO.saveCredito(credito);
    }
    
    public List<Persona> getAllPersonas() {
        return personaDAO.getAllPersonas();
    }
    
    public Credito findByNumeroCreditoByCobrador(String numeroCredito, Integer idCobrador) {
        return creditoDAO.findByNumeroCreditoByCobrador(numeroCredito, idCobrador);
    }
    
    public List<Credito> findCreditoByIdPersonaBySede(Long idPersona, Integer idSede) {
        return creditoDAO.findCreditoByIdPersonaBySede(idPersona, idSede);
    }
    
    public Persona findPersonaById(Long idPersona) {
        return personaDAO.findPersonaById(idPersona);
    }
    
    public Persona findPersonaByDocumento(String numeroDocumento) {
        return personaDAO.findPersonaByDocumento(numeroDocumento);
    }
    
    public void crearPagos(Credito credito) {
        creditoDAO.crearPagos(credito);
    }
    
    /**
     * Holds value of property sedeDAO.
     */
    private SedeDAOImpl sedeDAO;

    /**
     * Getter for property sedeDAO.
     * @return Value of property sedeDAO.
     */
    public SedeDAOImpl getSedeDAO() {
        return this.sedeDAO;
    }

    /**
     * Setter for property sedeDAO.
     * @param sedeDAO New value of property sedeDAO.
     */
    public void setSedeDAO(SedeDAOImpl sedeDAO) {
        this.sedeDAO = sedeDAO;
    }

    /**
     * Holds value of property creditoDAO.
     */
    private CreditoDAOImpl creditoDAO;

    /**
     * Getter for property creditoDAO.
     * @return Value of property creditoDAO.
     */
    public CreditoDAOImpl getCreditoDAO() {
        return this.creditoDAO;
    }

    /**
     * Setter for property creditoDAO.
     * @param creditoDAO New value of property creditoDAO.
     */
    public void setCreditoDAO(CreditoDAOImpl creditoDAO) {
        this.creditoDAO = creditoDAO;
    }

    /**
     * Holds value of property personaDAO.
     */
    private PersonaDAOImpl personaDAO;

    /**
     * Getter for property personaDAO.
     * @return Value of property personaDAO.
     */
    public PersonaDAOImpl getPersonaDAO() {
        return this.personaDAO;
    }

    /**
     * Setter for property personaDAO.
     * @param personaDAO New value of property personaDAO.
     */
    public void setPersonaDAO(PersonaDAOImpl personaDAO) {
        this.personaDAO = personaDAO;
    }

    /**
     * Holds value of property cobradorDAO.
     */
    private CobradorDAOImpl cobradorDAO;

    /**
     * Getter for property cobradorDAO.
     * @return Value of property cobradorDAO.
     */
    public CobradorDAOImpl getCobradorDAO() {
        return this.cobradorDAO;
    }

    /**
     * Setter for property cobradorDAO.
     * @param cobradorDAO New value of property cobradorDAO.
     */
    public void setCobradorDAO(CobradorDAOImpl cobradorDAO) {
        this.cobradorDAO = cobradorDAO;
    }

    /**
     * Holds value of property usuarioDAO.
     */
    private UsuarioDAOImpl usuarioDAO;

    /**
     * Getter for property usuarioDAO.
     * @return Value of property usuarioDAO.
     */
    public UsuarioDAOImpl getUsuarioDAO() {
        return this.usuarioDAO;
    }

    /**
     * Setter for property usuarioDAO.
     * @param usuarioDAO New value of property usuarioDAO.
     */
    public void setUsuarioDAO(UsuarioDAOImpl usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    /**
     * Holds value of property reporteDAO.
     */
    private ReporteDAOImpl reporteDAO;

    /**
     * Getter for property reporteDAO.
     * @return Value of property reporteDAO.
     */
    public ReporteDAOImpl getReporteDAO() {
        return this.reporteDAO;
    }

    /**
     * Setter for property reporteDAO.
     * @param reporteDAO New value of property reporteDAO.
     */
    public void setReporteDAO(ReporteDAOImpl reporteDAO) {
        this.reporteDAO = reporteDAO;
    }
    public Cobrador findCobradorByUsuario(String usuario){
         return cobradorDAO.findCobradorByUsuario(usuario);
     }

    public List<Integer> findNumerosDisponibles(Integer idCobrador){
        return creditoDAO.findNumerosDisponibles(idCobrador);
    }
    public List<Integer> findNumerosAsignados(Integer idCobrador){
        return creditoDAO.findNumerosAsignados(idCobrador);
    }
    
}
