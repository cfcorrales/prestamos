/*
 * Test.java
 *
 * Created on 18 de abril de 2007, 02:19 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pre.model.vo.Credito;
import pre.model.vo.Persona;
import pre.services.PrestamoServiceImpl;

/**
 *
 * @author jdgomez
 */
public class Test {
    
    /** Creates a new instance of Test */
    public Test() {
    }
    
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[] {  "/spring/applicationContext-txlocal.xml",
                                                                                    "/spring/applicationContext-hibernate.xml",
                                                                                    "/spring/applicationContext.xml"});
        PrestamoServiceImpl prestamoService = (PrestamoServiceImpl)ctx.getBean("prestamoService");

        /*Persona persona = new Persona();
        persona.setDocumento("11");
        persona.setNombre("a");
        persona.setApellidos("b");
        Credito credito = new Credito();
        
        credito.setNumeroCredito("4");
        prestamoService.saveCreditoPersona(credito, persona);*/
        
        //System.out.println(prestamoService.findByApellido("b").size());
                
    }
    
}
