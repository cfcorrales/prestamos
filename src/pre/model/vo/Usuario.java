/*
 * Usuario.java
 *
 * Created on 19 de mayo de 2007, 11:35 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

import java.util.Date;

/**
 *
 * @author jdgomez
 */
public class Usuario {
    /**
     * Holds value of property idUsuario.
     */
    private Integer idUsuario;

    /**
     * Getter for property idUsuario.
     * @return Value of property idUsuario.
     */
    public Integer getIdUsuario() {
        return this.idUsuario;
    }

    /**
     * Setter for property idUsuario.
     * @param idUsuario New value of property idUsuario.
     */
    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * Holds value of property nombreUsuario.
     */
    private String nombreUsuario;

    /**
     * Getter for property nombreUsuario.
     * @return Value of property nombreUsuario.
     */
    public String getNombreUsuario() {
        return this.nombreUsuario;
    }

    /**
     * Setter for property nombreUsuario.
     * @param nombreUsuario New value of property nombreUsuario.
     */
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    /**
     * Holds value of property login.
     */
    private String login;

    /**
     * Getter for property login.
     * @return Value of property login.
     */
    public String getLogin() {
        return this.login;
    }

    /**
     * Setter for property login.
     * @param login New value of property login.
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Holds value of property passwd.
     */
    private String passwd;

    /**
     * Getter for property passwd.
     * @return Value of property passwd.
     */
    public String getPasswd() {
        return this.passwd;
    }

    /**
     * Setter for property passwd.
     * @param passwd New value of property passwd.
     */
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    /**
     * Holds value of property fechaCreacion.
     */
    private Date fechaCreacion;

    /**
     * Getter for property fechaCreacion.
     * @return Value of property fechaCreacion.
     */
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    /**
     * Setter for property fechaCreacion.
     * @param fechaCreacion New value of property fechaCreacion.
     */
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    
}
