/*
 * PnlCreditos.java
 *
 * Created on 10 de marzo de 2007, 08:56 AM
 */

package pre.cliente.paneles;

import bsh.This;
import com.general.GeneralComboBoxModel;
import com.utils.MyException;
import com.utils.MyExceptionVal;
import com.utils.Texto;
import com.utils.Utilidades;
import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import org.jdesktop.swingx.JXPanel;
import pre.model.vo.Cobrador;
import pre.model.vo.Codeudor;
import pre.model.vo.Credito;
import pre.model.vo.Persona;
import pre.model.vo.Sede;
import pre.services.PrestamoServiceImpl;

/**
 *
 * @author  jdgomez
 */
public class PnlCreditos extends JXPanel {
    private PrestamoServiceImpl prestamoService;
    private Persona persona;
    private List<Credito> creditos;
    private Credito credito;
    private Long idCredito;
    private Codeudor codeudor1;
    private Codeudor codeudor2;
    private Cobrador cobrador;
    private Sede sede;
    private Map cobradoresMap = new HashMap();
    private Map numeros=new HashMap();
    final Integer DENOMINACIONMINIMA=50;
    
    /** Creates new form PnlCreditos */
    public PnlCreditos(PrestamoServiceImpl prestamoService, Sede sede) {
        UIManager.put("ComboBox.disabledForeground", Color.BLACK);
        UIManager.put("TextField.font", Color.BLACK);
        UIManager.put("JXDatePicker.disabledForeground", Color.BLACK);
        this.sede = sede;
        this.prestamoService = prestamoService;
        initComponents();
        //btnEliminar.setVisible(false);
        btnActivarCredito.setVisible(false);
        Utilidades.estado(jXPanel2, false);
    }
    
    private void cargarCobradores() {
        List<Cobrador> cobradores = prestamoService.getCobradoresBySede(sede.getIdSede());
        for (int i = 0; i < cobradores.size(); i++)
            cobradoresMap.put(cobradores.get(i).getIdCobrador(), cobradores.get(i));
        cmbCobrador.setModel(new GeneralComboBoxModel(cobradores));
    }
    
    public void setPersona(Persona persona) {
        cargarCobradores();
        this.persona = persona;
        credito = null;
        creditos = prestamoService.findCreditoByIdPersonaBySede(persona.getIdPersona(), sede.getIdSede());
        cmbCreditosFiltro.setModel(new GeneralComboBoxModel(creditos));
        cmbCreditosFiltro.setSelectedItem(0);
        Utilidades.limpiar(jXPanel2);
        Utilidades.estado(jXPanel2, false);
        Utilidades.estado(jPanel2, false);
        Utilidades.estado(jPanel3, false);
    }
    
    private void modelToViewCredito() {
        idCredito = credito.getIdCredito();
        cmbCobrador.setSelectedItem(cobradoresMap.get(credito.getIdCobrador()));
      //  txtNumeroCredito.setText(""+credito.getNumeroCredito());
        
        cmbNumeros.setSelectedItem(numeros.get(credito.getNumeroCredito()));
        dtpFechaPrestamo.setDate(credito.getFechaInicio());
        txtValorPrestado.setValue(credito.getValorPrestado());
        txtValorAPagar.setValue(credito.getValorTotalPagar());
        txtNumeroCuotas.setText(""+credito.getNumCuotas());
        txtValorCuota.setValue((credito.getValorCuota()==null?"":credito.getValorCuota()));
        cmbFrecuenciaCobro.setSelectedIndex(credito.getFrecuenciaCobro());
        txtSaldo.setValue(credito.getDeudaActual());
        if(null!=credito.getRuta())
            txtRuta.setText(credito.getRuta().toString());
    }
    
    private void viewToModelCredito() {
        credito.setIdCobrador(((Cobrador)cmbCobrador.getSelectedItem()).getIdCobrador());
        if(idCredito==null)
            credito.setNumeroCredito(cmbNumeros.getSelectedItem().toString());
        credito.setFechaInicio(dtpFechaPrestamo.getDate());
        credito.setValorPrestado(Long.parseLong(txtValorPrestado.getValue().toString()));
        credito.setValorTotalPagar(Long.parseLong(txtValorAPagar.getValue().toString()));
        credito.setNumCuotas(Integer.parseInt(txtNumeroCuotas.getText()));
        credito.setFrecuenciaCobro(cmbFrecuenciaCobro.getSelectedIndex());
        credito.setRuta(Integer.parseInt(txtRuta.getText()));
        Long valorCuota=credito.getValorTotalPagar()/credito.getNumCuotas();
        if(valorCuota%DENOMINACIONMINIMA==0) {
            credito.setValorCuota(valorCuota);
        }else{
            Long aprox=(valorCuota-valorCuota%DENOMINACIONMINIMA)+DENOMINACIONMINIMA;
            credito.setValorCuota(aprox);
        }

        //credito.setValorCuota(credito.getValorTotalPagar()/credito.getNumCuotas());

    }
    
    private void validarCredito() throws MyExceptionVal {
        //Utilidades.validar(cmbNumeros, "el n�mero del credito");

        if(cmbNumeros.getSelectedIndex()< 1 && idCredito==null)
             throw new MyExceptionVal("Debe seleccionar un numero de credito");
        if (cmbCobrador.getSelectedIndex() < 1)
            throw new MyExceptionVal("Debe seleccionar un cobrador");
        Integer idCobrador = ((Cobrador)cmbCobrador.getSelectedItem()).getIdCobrador();
        if (idCredito == null) {
            Credito cre = prestamoService.findByNumeroCreditoByCobrador(cmbNumeros.getSelectedItem().toString(), idCobrador);
            if (cre != null)
                throw new MyExceptionVal("Este n�mero de credito ya esta utilizado");
        }else {
            if (!credito.getNumeroCredito().equals((String)cmbNumeros.getSelectedItem())) {
                Credito cre = prestamoService.findByNumeroCreditoByCobrador((String)cmbNumeros.getSelectedItem(), idCobrador);
                if (cre != null)
                    throw new MyExceptionVal("Este n�mero de credito ya esta utilizado");
            }
        }
        if (cmbFrecuenciaCobro.getSelectedIndex() < 1)
            throw new MyExceptionVal("Debe seleccionar una frecuencia de cobro");
        Utilidades.validarFecha(dtpFechaPrestamo.getDate(), "la fecha del prestamo");
        Utilidades.validar(txtValorPrestado.getText(), "el valor del prestamo");
        Utilidades.validar(txtValorAPagar.getText(), "el valor a pagar");
        Utilidades.validar(txtNumeroCuotas.getText(), "el n�mero de cuotas");
        
    }
    
    private void modelToViewCodeudores() {
        //Codeudor 1
        txtDocumento.setText(codeudor1.getDocumento());
        txtNombre.setText(codeudor1.getNombre());
        txtDirCasa.setText(codeudor1.getDireccionCasa());
        txtTelCasa.setText(codeudor1.getTelefonoCasa());
        txtDirTrabajo.setText(codeudor1.getDireccionTrabajo());
        txtTelTrabajo.setText(codeudor1.getTelefonoTrabajo());
        txtCelular.setText(codeudor1.getCelular());
        //Codeudor 2
        txtDocumento1.setText(codeudor2.getDocumento());
        txtNombre1.setText(codeudor2.getNombre());
        txtDirCasa1.setText(codeudor2.getDireccionCasa());
        txtTelCasa1.setText(codeudor2.getTelefonoCasa());
        txtDirTrabajo1.setText(codeudor2.getDireccionTrabajo());
        txtTelTrabajo1.setText(codeudor2.getTelefonoTrabajo());
        txtCelular1.setText(codeudor2.getCelular());
    }
    
    private void viewToModelCodeudores() {
        //Codeudor 1
        codeudor1.setDocumento(txtDocumento.getText());
        codeudor1.setNombre(txtNombre.getText());
        codeudor1.setDireccionCasa(txtDirCasa.getText());
        codeudor1.setTelefonoCasa(txtTelCasa.getText());
        codeudor1.setDireccionTrabajo(txtDirTrabajo.getText());
        codeudor1.setTelefonoTrabajo(txtTelTrabajo.getText());
        codeudor1.setCelular(txtCelular.getText());
        //Codeudor 2
        codeudor2.setDocumento(txtDocumento1.getText());
        codeudor2.setNombre(txtNombre1.getText());
        codeudor2.setDireccionCasa(txtDirCasa1.getText());
        codeudor2.setTelefonoCasa(txtTelCasa1.getText());
        codeudor2.setDireccionTrabajo(txtDirTrabajo1.getText());
        codeudor2.setTelefonoTrabajo(txtTelTrabajo1.getText());
        codeudor2.setCelular(txtCelular1.getText());
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXTaskPaneContainer1 = new org.jdesktop.swingx.JXTaskPaneContainer();
        jXTitledPanel1 = new org.jdesktop.swingx.JXTitledPanel();
        jPanel1 = new javax.swing.JPanel();
        lblCreditosFiltro = new javax.swing.JLabel();
        cmbCreditosFiltro = new javax.swing.JComboBox();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jXPanel3 = new org.jdesktop.swingx.JXPanel();
        jXPanel5 = new org.jdesktop.swingx.JXPanel();
        btnAceptar1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtDirTrabajo = new javax.swing.JTextField();
        txtDocumento = new javax.swing.JTextField();
        lblDocumento = new javax.swing.JLabel();
        txtTelCasa = new javax.swing.JTextField();
        lblTelCasa = new javax.swing.JLabel();
        lblDirCasa = new javax.swing.JLabel();
        lblDirTrabajo = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblTelTrabajo = new javax.swing.JLabel();
        txtDirCasa = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtTelTrabajo = new javax.swing.JTextField();
        lblCelular = new javax.swing.JLabel();
        txtCelular = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        lblDocumento1 = new javax.swing.JLabel();
        txtDocumento1 = new javax.swing.JTextField();
        lblNombre1 = new javax.swing.JLabel();
        txtNombre1 = new javax.swing.JTextField();
        lblDirCasa1 = new javax.swing.JLabel();
        txtDirCasa1 = new javax.swing.JTextField();
        lblTelCasa1 = new javax.swing.JLabel();
        txtTelCasa1 = new javax.swing.JTextField();
        lblDirTrabajo1 = new javax.swing.JLabel();
        txtDirTrabajo1 = new javax.swing.JTextField();
        lblTelTrabajo1 = new javax.swing.JLabel();
        txtTelTrabajo1 = new javax.swing.JTextField();
        lblCelular1 = new javax.swing.JLabel();
        txtCelular1 = new javax.swing.JTextField();
        jXPanel2 = new org.jdesktop.swingx.JXPanel();
        lblNumeroCredito = new javax.swing.JLabel();
        lblFechaPrestamo = new javax.swing.JLabel();
        dtpFechaPrestamo = new org.jdesktop.swingx.JXDatePicker();
        lblValorPrestado = new javax.swing.JLabel();
        lblValorAPagar = new javax.swing.JLabel();
        lblNumeroCuotas = new javax.swing.JLabel();
        txtNumeroCuotas = new javax.swing.JTextField();
        lblFrecuenciaCobro = new javax.swing.JLabel();
        cmbFrecuenciaCobro = new javax.swing.JComboBox();
        lblValorCuota = new javax.swing.JLabel();
        lblSaldo = new javax.swing.JLabel();
        lblCobrador = new javax.swing.JLabel();
        cmbCobrador = new javax.swing.JComboBox();
        cmbNumeros = new javax.swing.JComboBox();
        txtValorPrestado = new javax.swing.JFormattedTextField();
        txtValorAPagar = new javax.swing.JFormattedTextField();
        txtSaldo = new javax.swing.JFormattedTextField();
        txtValorCuota = new javax.swing.JFormattedTextField();
        jLabel1 = new javax.swing.JLabel();
        txtRuta = new javax.swing.JTextField();
        btnNuevo = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnActivarCredito = new javax.swing.JButton();

        setAutoscrolls(true);
        setLayout(new java.awt.BorderLayout());

        jXTaskPaneContainer1.setAutoscrolls(true);

        jXTitledPanel1.setTitle("Creditos");
        jXTitledPanel1.getContentContainer().setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        lblCreditosFiltro.setText("Creditos");
        jPanel1.add(lblCreditosFiltro);

        cmbCreditosFiltro.setPreferredSize(new java.awt.Dimension(200, 20));
        cmbCreditosFiltro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCreditosFiltroActionPerformed(evt);
            }
        });
        jPanel1.add(cmbCreditosFiltro);

        jXTitledPanel1.getContentContainer().add(jPanel1, java.awt.BorderLayout.CENTER);

        jXTaskPaneContainer1.add(jXTitledPanel1);

        add(jXTaskPaneContainer1, java.awt.BorderLayout.NORTH);

        jTabbedPane1.setAutoscrolls(true);

        jXPanel3.setAutoscrolls(true);
        jXPanel3.setLayout(new java.awt.BorderLayout());

        jXPanel5.setAutoscrolls(true);
        jXPanel5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        btnAceptar1.setText("Aceptar");
        btnAceptar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptar1ActionPerformed(evt);
            }
        });
        jXPanel5.add(btnAceptar1);

        jXPanel3.add(jXPanel5, java.awt.BorderLayout.SOUTH);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Codeudor 1"));
        jPanel2.setAutoscrolls(true);
        jPanel2.setPreferredSize(new java.awt.Dimension(100, 140));

        txtDirTrabajo.setPreferredSize(new java.awt.Dimension(250, 18));

        txtDocumento.setDocument(new Texto(20, Texto.BIG_INTEGER));
        txtDocumento.setPreferredSize(new java.awt.Dimension(250, 18));

        lblDocumento.setText("Documento");

        txtTelCasa.setPreferredSize(new java.awt.Dimension(250, 18));

        lblTelCasa.setText("Tel�fono Casa");

        lblDirCasa.setText("Dir. Casa");

        lblDirTrabajo.setText("Dir. Trabajo");

        lblNombre.setText("Nombre");

        lblTelTrabajo.setText("Tel�fono Trabajo");

        txtDirCasa.setPreferredSize(new java.awt.Dimension(250, 18));

        txtNombre.setPreferredSize(new java.awt.Dimension(250, 18));

        txtTelTrabajo.setPreferredSize(new java.awt.Dimension(250, 18));

        lblCelular.setText("Celular");

        txtCelular.setMinimumSize(new java.awt.Dimension(6, 25));
        txtCelular.setPreferredSize(new java.awt.Dimension(250, 18));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDirCasa, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDirTrabajo)
                    .addComponent(lblCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDocumento, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                    .addComponent(txtDirCasa, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                    .addComponent(txtDirTrabajo, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                    .addComponent(txtCelular, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTelCasa, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTelTrabajo))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                    .addComponent(txtTelCasa, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                    .addComponent(txtTelTrabajo, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE))
                .addGap(31, 31, 31))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDirCasa, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDirCasa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTelCasa, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelCasa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDirTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDirTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTelTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jXPanel3.add(jPanel2, java.awt.BorderLayout.NORTH);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Codeudor 2"));
        jPanel3.setAutoscrolls(true);

        lblDocumento1.setText("Documento");

        txtDocumento1.setDocument(new Texto(20, Texto.BIG_INTEGER));
        txtDocumento1.setPreferredSize(new java.awt.Dimension(250, 18));

        lblNombre1.setText("Nombre");

        txtNombre1.setPreferredSize(new java.awt.Dimension(250, 18));

        lblDirCasa1.setText("Dir. Casa");

        txtDirCasa1.setPreferredSize(new java.awt.Dimension(250, 18));

        lblTelCasa1.setText("Tel�fono Casa");

        txtTelCasa1.setPreferredSize(new java.awt.Dimension(250, 18));

        lblDirTrabajo1.setText("Dir. Trabajo");

        txtDirTrabajo1.setPreferredSize(new java.awt.Dimension(250, 18));

        lblTelTrabajo1.setText("Tel�fono Trabajo");

        txtTelTrabajo1.setPreferredSize(new java.awt.Dimension(250, 18));

        lblCelular1.setText("Celular");

        txtCelular1.setMinimumSize(new java.awt.Dimension(6, 25));
        txtCelular1.setPreferredSize(new java.awt.Dimension(250, 18));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDocumento1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDirCasa1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDirTrabajo1)
                    .addComponent(lblCelular1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDocumento1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDirCasa1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDirTrabajo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtCelular1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTelCasa1, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTelTrabajo1))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTelTrabajo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTelCasa1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtNombre1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(31, 31, 31))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDocumento1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumento1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDirCasa1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDirCasa1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTelCasa1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelCasa1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDirTrabajo1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDirTrabajo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTelTrabajo1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelTrabajo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCelular1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCelular1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jXPanel3.add(jPanel3, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Codeudores", jXPanel3);

        jXPanel2.setAutoscrolls(true);

        lblNumeroCredito.setText("N�mero Credito");

        lblFechaPrestamo.setText("Fecha Prestamo");

        dtpFechaPrestamo.setPreferredSize(new java.awt.Dimension(6, 20));

        lblValorPrestado.setText("Valor Prestado");

        lblValorAPagar.setText("Valor a Pagar");

        lblNumeroCuotas.setText("N�mero Cuotas");

        txtNumeroCuotas.setDocument(new Texto(3, Texto.INTEGER_TYPE));

        lblFrecuenciaCobro.setText("Frecuencia Cobro");

        cmbFrecuenciaCobro.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " ", "Cada Semana", "Cada 2 Semanas", "Cada 4 Semanas", "Diario" }));
        cmbFrecuenciaCobro.setPreferredSize(new java.awt.Dimension(6, 20));

        lblValorCuota.setText("Valor Cuota");
        lblValorCuota.setPreferredSize(new java.awt.Dimension(6, 20));

        lblSaldo.setText("Saldo");
        lblSaldo.setPreferredSize(new java.awt.Dimension(6, 20));

        lblCobrador.setText("Cobrador");

        cmbCobrador.setPreferredSize(new java.awt.Dimension(6, 20));
        cmbCobrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCobradorActionPerformed(evt);
            }
        });

        cmbNumeros.setPreferredSize(new java.awt.Dimension(6, 20));

        txtValorPrestado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        txtValorAPagar.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        txtSaldo.setEditable(false);
        txtSaldo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        txtValorCuota.setEditable(false);
        txtValorCuota.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        jLabel1.setText("Num. Ruta:");

        txtRuta.setDocument(new Texto(15, Texto.INTEGER_TYPE));

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnActivarCredito.setText("Activar Credito");
        btnActivarCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActivarCreditoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jXPanel2Layout = new javax.swing.GroupLayout(jXPanel2);
        jXPanel2.setLayout(jXPanel2Layout);
        jXPanel2Layout.setHorizontalGroup(
            jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanel2Layout.createSequentialGroup()
                .addGap(95, 95, 95)
                .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNumeroCredito)
                    .addComponent(lblCobrador, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblValorPrestado, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNumeroCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jXPanel2Layout.createSequentialGroup()
                        .addComponent(btnNuevo)
                        .addGap(5, 5, 5)
                        .addComponent(btnAceptar)
                        .addGap(5, 5, 5)
                        .addComponent(btnCancelar)
                        .addGap(5, 5, 5)
                        .addComponent(btnEliminar)
                        .addGap(5, 5, 5)
                        .addComponent(btnActivarCredito))
                    .addGroup(jXPanel2Layout.createSequentialGroup()
                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cmbCobrador, javax.swing.GroupLayout.Alignment.LEADING, 0, 400, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jXPanel2Layout.createSequentialGroup()
                                .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtRuta, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
                                    .addComponent(txtValorPrestado, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
                                    .addComponent(cmbNumeros, 0, 148, Short.MAX_VALUE)
                                    .addComponent(txtNumeroCuotas, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
                                    .addComponent(txtSaldo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE))
                                .addGap(15, 15, 15)
                                .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lblFrecuenciaCobro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblValorAPagar, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)
                                    .addComponent(lblFechaPrestamo, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)
                                    .addComponent(lblValorCuota, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jXPanel2Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(txtValorCuota, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
                                    .addGroup(jXPanel2Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cmbFrecuenciaCobro, 0, 135, Short.MAX_VALUE)
                                            .addComponent(dtpFechaPrestamo, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                                            .addComponent(txtValorAPagar, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))))))
                        .addGap(3, 3, 3)))
                .addGap(136, 136, 136))
        );

        jXPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lblFechaPrestamo, lblFrecuenciaCobro, lblSaldo, lblValorAPagar});

        jXPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lblCobrador, lblNumeroCredito, lblNumeroCuotas, lblValorPrestado});

        jXPanel2Layout.setVerticalGroup(
            jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanel2Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCobrador, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cmbCobrador, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jXPanel2Layout.createSequentialGroup()
                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cmbNumeros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNumeroCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblValorPrestado, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtValorPrestado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblNumeroCuotas)
                            .addComponent(txtNumeroCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jXPanel2Layout.createSequentialGroup()
                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(dtpFechaPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblFechaPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblValorAPagar, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtValorAPagar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblFrecuenciaCobro, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbFrecuenciaCobro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtValorCuota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblValorCuota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtRuta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jXPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnNuevo)
                    .addComponent(btnAceptar)
                    .addComponent(btnCancelar)
                    .addComponent(btnEliminar)
                    .addComponent(btnActivarCredito))
                .addGap(97, 97, 97))
        );

        jXPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmbCobrador, cmbNumeros, dtpFechaPrestamo, lblFechaPrestamo, lblNumeroCredito});

        jXPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lblValorAPagar, lblValorPrestado, txtValorAPagar, txtValorPrestado});

        jXPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmbFrecuenciaCobro, lblFrecuenciaCobro, lblNumeroCuotas, txtNumeroCuotas});

        jXPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lblSaldo, lblValorCuota, txtSaldo, txtValorCuota});

        jXPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, txtRuta});

        jTabbedPane1.addTab("Informaci�n Credito", jXPanel2);

        add(jTabbedPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void btnActivarCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActivarCreditoActionPerformed
        // TODO add your handling code here:
        if (JOptionPane.showConfirmDialog(this, "Esta seguro que desea activar este credito?, recuerde que despu�s de activado no podr� modificarlo", "Activar", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE) == JOptionPane.OK_OPTION) {
            prestamoService.activarCredito(credito);
            //btnEliminar.setVisible(false);
            btnActivarCredito.setVisible(false);
            Utilidades.estado(jXPanel2, false);
        }
    }//GEN-LAST:event_btnActivarCreditoActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        if (credito != null) {
            if (credito.getEstado().intValue() != 0) {//estado cero es para creditos terminados los cuales no se deben eliminar
              if (JOptionPane.showConfirmDialog(this, "Esta seguro que desea eliminar este credito?", "Eliminar", JOptionPane.OK_CANCEL_OPTION, JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
                prestamoService.removeCredito(credito);
                setPersona(persona);
                JOptionPane.showMessageDialog(this, "Cr�dito eliminado correctamente!!!", "Confirmaci�n", JOptionPane.INFORMATION_MESSAGE);
                //btnEliminar.setVisible(false);
                btnActivarCredito.setVisible(false);

              }
            }
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnAceptar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptar1ActionPerformed
        // TODO add your handling code here:
        if (codeudor1 != null && codeudor2 != null) {
            viewToModelCodeudores();
            prestamoService.saveCredito(credito);
        }
    }//GEN-LAST:event_btnAceptar1ActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        // TODO add your handling code here:
       // if (idCredito != null && credito.getEstado() < 2)
       //     return;
        try {
            validarCredito();
            if (idCredito == null) {
                credito = new Credito();
                codeudor1 = new Codeudor();
                codeudor1.setNumeroCodeudor(1);
                codeudor2 = new Codeudor();
                codeudor2.setNumeroCodeudor(2);
                credito.setIdPersona(persona.getIdPersona());
                credito.getCodeudores().add(codeudor1);
                credito.getCodeudores().add(codeudor2);
                credito.setEstado(2);
                credito.setDeudaActual((Long)txtValorAPagar.getValue());
            }
            viewToModelCredito();
            prestamoService.saveCredito(credito);
            setPersona(persona);
            JOptionPane.showMessageDialog(this,"El cr�dito se guard� exitosamente", "Confirmaci�n",  JOptionPane.INFORMATION_MESSAGE);
        }catch(MyExceptionVal e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Guardar", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        if (credito != null) {
            modelToViewCredito();
            Utilidades.estado(jXPanel2, credito.getEstado().intValue()==2);
        }else {
            Utilidades.limpiar(jXPanel2);
            Utilidades.estado(jXPanel2, false);
        }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        if (persona == null) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar primero una persona");
            return;
        }
        //btnEliminar.setVisible(false);
        btnActivarCredito.setVisible(false);
        
        idCredito = null;
        Utilidades.limpiar(jXPanel2);
        Utilidades.estado(jXPanel2, true);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void cmbCreditosFiltroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCreditosFiltroActionPerformed
        // TODO add your handling code here:
        if (cmbCreditosFiltro.getSelectedIndex() <= 0) {
            Utilidades.estado(jXPanel2, false);
            Utilidades.limpiar(jXPanel2);
            Utilidades.limpiar(jPanel2);
            Utilidades.estado(jPanel2, false);
            Utilidades.limpiar(jPanel3);
            Utilidades.estado(jPanel3, false);
            credito = null;
            idCredito = null;
            //btnEliminar.setVisible(false);
            return;
        }
        credito = (Credito)cmbCreditosFiltro.getSelectedItem();
        if (credito != null) {
            modelToViewCredito();
            Iterator<Codeudor> it = credito.getCodeudores().iterator();
            codeudor1 = it.next();
            codeudor2 = it.next();
            modelToViewCodeudores();
            Utilidades.estado(jXPanel2, credito.getEstado().intValue()==2);
            txtRuta.setEditable(true);
            txtRuta.setEnabled(true);
            btnAceptar.setEnabled(true);
            cmbCobrador.setEnabled(true);
            Utilidades.estado(jPanel2, true);
            Utilidades.estado(jPanel3, true);
            btnEliminar.setVisible(credito.getEstado().intValue()!=0);
            btnActivarCredito.setVisible(credito.getEstado().intValue()==2);
        }
    }//GEN-LAST:event_cmbCreditosFiltroActionPerformed

    private void cmbCobradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCobradorActionPerformed
        
        if(cmbCobrador.getSelectedItem()!=null){
            List<Integer> lst=prestamoService.findNumerosDisponibles(((Cobrador)cmbCobrador.getSelectedItem()).getIdCobrador());
        
        for(Integer num:lst)
            numeros.put(num, num);
        cmbNumeros.setModel(new GeneralComboBoxModel(lst));
        }
    }//GEN-LAST:event_cmbCobradorActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnAceptar1;
    private javax.swing.JButton btnActivarCredito;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox cmbCobrador;
    private javax.swing.JComboBox cmbCreditosFiltro;
    private javax.swing.JComboBox cmbFrecuenciaCobro;
    private javax.swing.JComboBox cmbNumeros;
    private org.jdesktop.swingx.JXDatePicker dtpFechaPrestamo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private org.jdesktop.swingx.JXPanel jXPanel2;
    private org.jdesktop.swingx.JXPanel jXPanel3;
    private org.jdesktop.swingx.JXPanel jXPanel5;
    private org.jdesktop.swingx.JXTaskPaneContainer jXTaskPaneContainer1;
    private org.jdesktop.swingx.JXTitledPanel jXTitledPanel1;
    private javax.swing.JLabel lblCelular;
    private javax.swing.JLabel lblCelular1;
    private javax.swing.JLabel lblCobrador;
    private javax.swing.JLabel lblCreditosFiltro;
    private javax.swing.JLabel lblDirCasa;
    private javax.swing.JLabel lblDirCasa1;
    private javax.swing.JLabel lblDirTrabajo;
    private javax.swing.JLabel lblDirTrabajo1;
    private javax.swing.JLabel lblDocumento;
    private javax.swing.JLabel lblDocumento1;
    private javax.swing.JLabel lblFechaPrestamo;
    private javax.swing.JLabel lblFrecuenciaCobro;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblNombre1;
    private javax.swing.JLabel lblNumeroCredito;
    private javax.swing.JLabel lblNumeroCuotas;
    private javax.swing.JLabel lblSaldo;
    private javax.swing.JLabel lblTelCasa;
    private javax.swing.JLabel lblTelCasa1;
    private javax.swing.JLabel lblTelTrabajo;
    private javax.swing.JLabel lblTelTrabajo1;
    private javax.swing.JLabel lblValorAPagar;
    private javax.swing.JLabel lblValorCuota;
    private javax.swing.JLabel lblValorPrestado;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtCelular1;
    private javax.swing.JTextField txtDirCasa;
    private javax.swing.JTextField txtDirCasa1;
    private javax.swing.JTextField txtDirTrabajo;
    private javax.swing.JTextField txtDirTrabajo1;
    private javax.swing.JTextField txtDocumento;
    private javax.swing.JTextField txtDocumento1;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNombre1;
    private javax.swing.JTextField txtNumeroCuotas;
    private javax.swing.JTextField txtRuta;
    private javax.swing.JFormattedTextField txtSaldo;
    private javax.swing.JTextField txtTelCasa;
    private javax.swing.JTextField txtTelCasa1;
    private javax.swing.JTextField txtTelTrabajo;
    private javax.swing.JTextField txtTelTrabajo1;
    private javax.swing.JFormattedTextField txtValorAPagar;
    private javax.swing.JFormattedTextField txtValorCuota;
    private javax.swing.JFormattedTextField txtValorPrestado;
    // End of variables declaration//GEN-END:variables
    
}
