/*
 * PnlCobradores.java
 *
 * Created on 30 de abril de 2007, 09:03 AM
 */

package pre.cliente.paneles;

import com.general.GeneralComboBoxModel;
import com.utils.MyExceptionVal;
import com.utils.Utilidades;
import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import org.jdesktop.swingx.JXPanel;
import pre.model.vo.Cobrador;
import pre.model.vo.Persona;
import pre.model.vo.Sede;
import pre.services.PrestamoServiceImpl;

/**
 *
 * @author  jdgomez
 */
public class PnlCobradores extends JXPanel {
    private PrestamoServiceImpl prestamoService;
    private Cobrador cobrador;
    private Persona persona;
    private PnlAdminPersona padre;
    private List<Cobrador> cobradores;
    private Integer idCobrador;
    private Sede sede;
    private Map sedesMap = new HashMap();
    private Map personasMap = new HashMap();
    
    /** Creates new form PnlCobradores */
    public PnlCobradores(PrestamoServiceImpl prestamoService, Sede sede) {
        UIManager.put("ComboBox.disabledForeground", Color.BLACK);
        UIManager.put("TextField.font", Color.BLACK);
        UIManager.put("JXDatePicker.disabledForeground", Color.BLACK);

        this.sede = sede;
        this.prestamoService = prestamoService;
        initComponents();
        Utilidades.estado(jPanel1, false);
        cobradores = prestamoService.getCobradoresBySede(sede.getIdSede());
        cmbCobradores.setModel(new GeneralComboBoxModel(cobradores));
        cargarSedes();
    }
    
    public void actualizar() {
        cargarPersonas();
    }
    
    private void cargarPersonas() {
        List<Persona> personas = prestamoService.getAllPersonas();
        for (int i = 0; i < personas.size(); i++)
            personasMap.put(personas.get(i).getIdPersona(), personas.get(i));
        cmbPersonas.setModel(new GeneralComboBoxModel(personas));
    }
    
    private void cargarSedes() {
        List<Sede> sedes = prestamoService.getSedes();
        for (int i = 0; i < sedes.size(); i++)
            sedesMap.put(sedes.get(i).getIdSede(), sedes.get(i));
        cmbSede.setModel(new GeneralComboBoxModel(sedes));
    }
    
    private void modelToView() {
        idCobrador = cobrador.getIdCobrador();
        persona = cobrador.getPersona();
        txtZona.setText(""+cobrador.getZona());
        cmbSede.setSelectedItem(sedesMap.get(cobrador.getIdSede()));
        cmbPersonas.setSelectedItem(personasMap.get(cobrador.getPersona().getIdPersona()));
        if(cobrador.getUsuario()!=null)
            txtUsuario.setText(cobrador.getUsuario());
        else
            txtUsuario.setText(null);
        if(cobrador.getPassword()!=null){
            txtPassword.setText(cobrador.getPassword());
            txtPassword2.setText(cobrador.getPassword());
        }else{
            txtPassword.setText(null);
            txtPassword2.setText(null);
        }
        if(cobrador.getEstado()!=null){
            chkActivo.setSelected(cobrador.getEstado()==1?true:false);
            }
        else{
            chkActivo.setSelected(false);
            }
            
        
    }
    
    private void viewToModel() {
        cobrador.setPersona((Persona)cmbPersonas.getSelectedItem());
        cobrador.setZona(txtZona.getText());
        cobrador.setIdSede(((Sede)cmbSede.getSelectedItem()).getIdSede());
        cobrador.setUsuario(txtUsuario.getText());
        cobrador.setPassword(txtPassword.getText());
        Integer estado=chkActivo.isSelected()?1:0;
        cobrador.setEstado(estado);
    }
    
    private void validar() throws MyExceptionVal {
        Utilidades.validar(txtZona.getText(), "la zona");
        if (cmbSede.getSelectedIndex() <= 0)
            throw new MyExceptionVal("Debe seleccionar una sede");
        if (cmbPersonas.getSelectedIndex() <= 0)
            throw new MyExceptionVal("Debe seleccionar una persona");
        Utilidades.validar(txtUsuario.getText(), "el usuario");
        Utilidades.validar(txtPassword.getText(), "el password");

         if(txtUsuario.getText().length()<=2){
              throw new MyExceptionVal("Error, El nombre de usuario ingresado es demasiado corto, debe ser mayor de 2 caracteres");
        }
        Cobrador cob = prestamoService.findCobradorByUsuario(txtUsuario.getText());
        if(cob!=null){
            if(!cob.getIdCobrador().equals(cobrador.getIdCobrador()))
                throw new MyExceptionVal("Error, El nombre de usuario ingresado ya est� asignado a otro cobrador.");
        }
        if(!txtPassword.getText().equals(txtPassword2.getText())){
              throw new MyExceptionVal("Error, los password deben ser iguales.");
        }
        if(txtPassword.getText().length()<=3){
             throw new MyExceptionVal("Error, el password debe contener mas de 3 caracteres.");
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXTaskPaneContainer1 = new org.jdesktop.swingx.JXTaskPaneContainer();
        jXTitledPanel1 = new org.jdesktop.swingx.JXTitledPanel();
        lblCobradores = new javax.swing.JLabel();
        cmbCobradores = new javax.swing.JComboBox();
        jXTitledPanel2 = new org.jdesktop.swingx.JXTitledPanel();
        jPanel1 = new javax.swing.JPanel();
        jXPanel2 = new org.jdesktop.swingx.JXPanel();
        btnNuevo = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        lblSede = new javax.swing.JLabel();
        cmbSede = new javax.swing.JComboBox();
        lblZona = new javax.swing.JLabel();
        txtZona = new javax.swing.JTextField();
        lblPersonas = new javax.swing.JLabel();
        cmbPersonas = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtPassword = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtPassword2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        chkActivo = new javax.swing.JCheckBox();

        setLayout(new java.awt.BorderLayout());

        jXTaskPaneContainer1.setLayout(new java.awt.BorderLayout(10, 10));

        jXTitledPanel1.setTitle("Cobradores");
        jXTitledPanel1.getContentContainer().setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        lblCobradores.setText("Cobradores");
        jXTitledPanel1.getContentContainer().add(lblCobradores);

        cmbCobradores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCobradoresActionPerformed(evt);
            }
        });
        jXTitledPanel1.getContentContainer().add(cmbCobradores);

        jXTaskPaneContainer1.add(jXTitledPanel1, java.awt.BorderLayout.NORTH);

        jXTitledPanel2.setTitle("Datos");
        jXTitledPanel2.getContentContainer().setLayout(new java.awt.BorderLayout());

        jXPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jXPanel2.add(btnNuevo);

        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        jXPanel2.add(btnAceptar);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jXPanel2.add(btnCancelar);

        lblSede.setText("Sede");

        cmbSede.setMinimumSize(new java.awt.Dimension(23, 25));
        cmbSede.setPreferredSize(new java.awt.Dimension(150, 25));

        lblZona.setText("Zona");

        txtZona.setMinimumSize(new java.awt.Dimension(6, 25));
        txtZona.setPreferredSize(new java.awt.Dimension(6, 25));

        lblPersonas.setText("Personas");

        cmbPersonas.setPreferredSize(new java.awt.Dimension(450, 25));

        jLabel1.setText("Usuario:");

        jLabel2.setText("Password:");

        jLabel3.setText("Redigite Password:");

        jLabel4.setText("Activo:");
        jLabel4.setPreferredSize(new java.awt.Dimension(92, 14));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jXPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 1088, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSede, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPersonas, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbPersonas, 0, 901, Short.MAX_VALUE)
                            .addComponent(cmbSede, 0, 901, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addComponent(lblZona, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(txtZona, javax.swing.GroupLayout.DEFAULT_SIZE, 901, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 901, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(txtPassword, javax.swing.GroupLayout.DEFAULT_SIZE, 901, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPassword2, javax.swing.GroupLayout.DEFAULT_SIZE, 901, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(chkActivo)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3, lblPersonas, lblSede, lblZona});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSede, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbSede, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPersonas, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbPersonas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblZona, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtZona, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(txtPassword2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkActivo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmbPersonas, cmbSede, jLabel3, txtPassword, txtPassword2, txtUsuario, txtZona});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel2, lblPersonas, lblSede});

        jXTitledPanel2.getContentContainer().add(jPanel1, java.awt.BorderLayout.CENTER);

        jXTaskPaneContainer1.add(jXTitledPanel2, java.awt.BorderLayout.CENTER);

        add(jXTaskPaneContainer1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void cmbCobradoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCobradoresActionPerformed
        // TODO add your handling code here:
        if (cmbCobradores.getSelectedIndex() <= 0) {
            Utilidades.limpiar(jPanel1);
            Utilidades.estado(jPanel1, false);
            idCobrador = null;
            cobrador = null;
            return;
        }
        cobrador = (Cobrador)cmbCobradores.getSelectedItem();
        if (cobrador != null) {
            modelToView();
            Utilidades.estado(jPanel1, true);
        }
    }//GEN-LAST:event_cmbCobradoresActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        if (persona != null) {
            modelToView();
        }else {
            Utilidades.limpiar(jPanel1);
            Utilidades.estado(jPanel1, false);
        }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        // TODO add your handling code here:
        try {
            validar();
            if (idCobrador == null) {
                cobrador = new Cobrador();
                cobrador.setPersona(persona);
            }
            viewToModel();
            prestamoService.saveCobrador(cobrador);
            cobradores = prestamoService.getCobradoresBySede(sede.getIdSede());
            Utilidades.estado(jPanel1, false);
            Utilidades.limpiar(jPanel1);
            cmbCobradores.setModel(new GeneralComboBoxModel(cobradores));
        }catch(MyExceptionVal e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Guardar", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        Utilidades.limpiar(jPanel1);
        idCobrador = null;
        Utilidades.estado(jPanel1, true);
    }//GEN-LAST:event_btnNuevoActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JCheckBox chkActivo;
    private javax.swing.JComboBox cmbCobradores;
    private javax.swing.JComboBox cmbPersonas;
    private javax.swing.JComboBox cmbSede;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private org.jdesktop.swingx.JXPanel jXPanel2;
    private org.jdesktop.swingx.JXTaskPaneContainer jXTaskPaneContainer1;
    private org.jdesktop.swingx.JXTitledPanel jXTitledPanel1;
    private org.jdesktop.swingx.JXTitledPanel jXTitledPanel2;
    private javax.swing.JLabel lblCobradores;
    private javax.swing.JLabel lblPersonas;
    private javax.swing.JLabel lblSede;
    private javax.swing.JLabel lblZona;
    private javax.swing.JTextField txtPassword;
    private javax.swing.JTextField txtPassword2;
    private javax.swing.JTextField txtUsuario;
    private javax.swing.JTextField txtZona;
    // End of variables declaration//GEN-END:variables
    
}
