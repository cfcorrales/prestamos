/*
 * Persona.java
 *
 * Created on 23 de febrero de 2007, 09:33 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author jdgomez
 */
public class Persona {

    /**
     * Holds value of property idPersona.
     */
    private Long idPersona;
    private String seudonimo;

    public String getSeudonimo() {
        return seudonimo;
    }

    public void setSeudonimo(String seudonimo) {
        this.seudonimo = seudonimo;
    }

    
    /**
     * Getter for property idPersona.
     * @return Value of property idPersona.
     */
    public Long getIdPersona() {
        return this.idPersona;
    }

    /**
     * Setter for property idPersona.
     * @param idPersona New value of property idPersona.
     */
    public void setIdPersona(Long idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * Holds value of property documento.
     */
    private String documento;

    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public String getDocumento() {
        return this.documento;
    }

    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * Holds value of property nombre.
     */
    private String nombre;

    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Holds value of property apellidos.
     */
    private String apellidos;

    /**
     * Getter for property apellido.
     * @return Value of property apellido.
     */
    public String getApellidos() {
        return this.apellidos;
    }

    /**
     * Setter for property apellido.
     * @param apellido New value of property apellido.
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Holds value of property telefonoCasa.
     */
    private String telefonoCasa;

    /**
     * Getter for property telefonoCasa.
     * @return Value of property telefonoCasa.
     */
    public String getTelefonoCasa() {
        return this.telefonoCasa;
    }

    /**
     * Setter for property telefonoCasa.
     * @param telefonoCasa New value of property telefonoCasa.
     */
    public void setTelefonoCasa(String telefonoCasa) {
        this.telefonoCasa = telefonoCasa;
    }

    /**
     * Holds value of property telefonoTrabajo.
     */
    private String telefonoTrabajo;

    /**
     * Getter for property telefonoTrabajo.
     * @return Value of property telefonoTrabajo.
     */
    public String getTelefonoTrabajo() {
        return this.telefonoTrabajo;
    }

    /**
     * Setter for property telefonoTrabajo.
     * @param telefonoTrabajo New value of property telefonoTrabajo.
     */
    public void setTelefonoTrabajo(String telefonoTrabajo) {
        this.telefonoTrabajo = telefonoTrabajo;
    }

    /**
     * Holds value of property celular.
     */
    private String celular;

    /**
     * Getter for property celular.
     * @return Value of property celular.
     */
    public String getCelular() {
        return this.celular;
    }

    /**
     * Setter for property celular.
     * @param celular New value of property celular.
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * Holds value of property direccionCasa.
     */
    private String direccionCasa;

    /**
     * Getter for property direccionCasa.
     * @return Value of property direccionCasa.
     */
    public String getDireccionCasa() {
        return this.direccionCasa;
    }

    /**
     * Setter for property direccionCasa.
     * @param direccionCasa New value of property direccionCasa.
     */
    public void setDireccionCasa(String direccionCasa) {
        this.direccionCasa = direccionCasa;
    }

    /**
     * Holds value of property direccionTrabajo.
     */
    private String direccionTrabajo;

    /**
     * Getter for property direccionTrabajo.
     * @return Value of property direccionTrabajo.
     */
    public String getDireccionTrabajo() {
        return this.direccionTrabajo;
    }

    /**
     * Setter for property direccionTrabajo.
     * @param direccionTrabajo New value of property direccionTrabajo.
     */
    public void setDireccionTrabajo(String direccionTrabajo) {
        this.direccionTrabajo = direccionTrabajo;
    }

    /**
     * Holds value of property estado.
     */
    private Integer estado;

    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public Integer getEstado() {
        return this.estado;
    }

    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    /**
     * Holds value of property fechaCreacion.
     */
    private Date fechaCreacion;

    /**
     * Getter for property fechaCreacion.
     * @return Value of property fechaCreacion.
     */
    public Date getFechaCreacion() {
        return new Date();
    }

    /**
     * Setter for property fechaCreacion.
     * @param fechaCreacion New value of property fechaCreacion.
     */
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    /**
     * Holds value of property propiedades.
     */
    private Set<Propiedad> propiedades = new HashSet<Propiedad>();

    /**
     * Getter for property propiedades.
     * @return Value of property propiedades.
     */
    public Set<Propiedad> getPropiedades() {
        return this.propiedades;
    }

    /**
     * Setter for property propiedades.
     * @param propiedades New value of property propiedades.
     */
    public void setPropiedades(Set<Propiedad> propiedades) {
        this.propiedades = propiedades;
    }
    
    public String toString() {
        return nombre + " " + apellidos;
    }
    
}
