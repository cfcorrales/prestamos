/*
 * CreditoDAOImpl.java
 *
 * Created on 31 de marzo de 2007, 08:50 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.dao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import org.aspectj.weaver.ast.ITestVisitor;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.springside.core.dao.HibernateEntityDao;
import pre.model.vo.Credito;
import pre.model.vo.Pago;

/**
 *
 * @author jdgomez
 */
public class CreditoDAOImpl extends HibernateEntityDao<Credito> {    
    
    public void saveCredito(Credito credito) {
        Long valorPagado = 0L;
        for(Iterator<Pago> it = credito.getPagos().iterator(); it.hasNext(); ) {
            Pago pago = it.next();
            if (pago.getValorPagado() != null)
                valorPagado = valorPagado + pago.getValorPagado();
        }
        credito.setDeudaActual(credito.getValorTotalPagar() - valorPagado);
        if (credito.getDeudaActual() <= 0) {
            credito.setEstado(0);
            credito.setNumeroCredito(maxNumCredito(credito.getIdCobrador()));
        }
        save(credito);
    }
    
    public String maxNumCredito(Integer idCobrador) {
        String qry = "select max(cast(NUMCREDITO as unsigned)) max from CREDITOS where IDCOBRADOR = ?";
        Long max = (Long)getSession().createSQLQuery(qry).addScalar("max", Hibernate.LONG).setInteger(0, idCobrador).uniqueResult();
        if (max < 4000)
            max = 4000L;
        else
            max = max + 1;
        return ""+max;
    }
    
    public Credito findByNumeroCreditoByCobrador(String numeroCredito, Integer idCobrador) {
        Criteria criteria = getSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("numeroCredito", numeroCredito));
        criteria.add(Restrictions.eq("idCobrador", idCobrador));
        return (Credito)criteria.uniqueResult();
    }
    
    public List<Credito> findCreditoByIdPersonaBySede(Long idPersona, Integer idSede) {
        String sql = "SELECT IDCREDITO, NUMCREDITO, A.IDCOBRADOR, A.IDPERSONA, NUMCUOTA, VALORCUOTA, VALORPRESTADO, VALORTOTALPAGAR, FRECUENCIACOBRO, DEUDAACTUAL, FECHAINICIO, CUOTAATRASO, A.ESTADO, A.FECHACREACION, INTERES, RUTA FROM CREDITOS A JOIN COBRADORES B ON (A.IDCOBRADOR = B.IDCOBRADOR) JOIN SEDES C ON (B.IDSEDE = C.IDSEDE) WHERE A.IDPERSONA = ? AND C.IDSEDE = ? ORDER BY FECHAINICIO DESC, NUMCREDITO ASC";
        return getSession().createSQLQuery(sql).addEntity(Credito.class).setLong(0, idPersona).setInteger(1, idSede).list();
    }
    
    public void crearPagos(Credito credito) {
        GregorianCalendar gCal = new GregorianCalendar();
        gCal.setTime(credito.getFechaInicio());
        int frecuencia=0;// = credito.getFrecuenciaCobro()==3?4:credito.getFrecuenciaCobro();
        if(credito.getFrecuenciaCobro()==3)
            frecuencia=4;
        else
            frecuencia=credito.getFrecuenciaCobro();//en el caso que credito.getFrecuenciaCobro() sea igual a 4 no importa porque en el if de mas abajo se valida este dato y no se suma la frecuencia sino un 1
        
        for (int i = 1; i <= credito.getNumCuotas(); i++) {
            Pago pago = new Pago();
            pago.setNumPago(i);
            pago.setEstado(2);
            if(credito.getFrecuenciaCobro()==4){//DIARIO
                gCal.add(Calendar.DAY_OF_MONTH, 1);
              }
            else if(credito.getFrecuenciaCobro()==2){//QUINCENAL
                    gCal.add(Calendar.DAY_OF_MONTH, 15);
                 }
            else if(credito.getFrecuenciaCobro()==3){//CADA 4 SEMANAS
                   gCal.add(Calendar.DAY_OF_MONTH, 30);
                 }
            else if(credito.getFrecuenciaCobro()==1){//CADA 1 SEMANA
                   gCal.add(Calendar.DAY_OF_MONTH, 7);
                 }
                
            pago.setFechaPago(gCal.getTime());
            pago.setValorAPagar(credito.getValorCuota());
            pago.setFrecuenciaCobro(frecuencia);

            credito.getPagos().add(pago);
        }
    }
    
    public void removeCredito(Credito credito) {
        remove(credito);
    }
    
    public List<Credito> findCredito(String hql, Object[] values) {
        return find(hql, values);
    }

    public List<Integer> findNumerosDisponibles(Integer idCobrador){

        String sql="SELECT NUMCREDITO FROM CREDITOS WHERE IDCOBRADOR="+idCobrador+" AND ESTADO IN(1,2) ORDER BY NUMCREDITO";
        List<String> lstString=getSession().createSQLQuery(sql).addScalar("NUMCREDITO").list();
        List<Integer> lstInteger = new ArrayList<Integer>();
        for(String numero:lstString){
            lstInteger.add(Integer.valueOf(numero));
        }
        Collections.sort(lstInteger);
        List<Integer> lstFaltantes=new ArrayList<Integer>();
        for(int i=1; i < lstInteger.size()-1;i++){
            int res=lstInteger.get(i+1) - lstInteger.get(i);
            for(int j=1;j<res;j++){
                int faltante=lstInteger.get(i+1)-j;
                lstFaltantes.add(faltante);
            }
        }
        int adicional;
        if(lstInteger!=null && lstInteger.size()<=0){
            adicional=1;
        }else{
            adicional=lstInteger.get(lstInteger.size()-1)+1;
        }
        
        lstFaltantes.add(adicional);//agrego el numero mayor de los disponibles mas uno para el caso que no haya numeros disponible
        Collections.sort(lstFaltantes);
        
        return lstFaltantes;

    }


    public List<Integer> findNumerosAsignados(Integer idCobrador){

        String sql="SELECT NUMCREDITO FROM CREDITOS WHERE IDCOBRADOR="+idCobrador+" ORDER BY NUMCREDITO";
        List<String> lstString=getSession().createSQLQuery(sql).addScalar("NUMCREDITO").list();
        List<Integer> lstInteger = new ArrayList<Integer>();
        for(String numero:lstString){
            lstInteger.add(Integer.valueOf(numero));
        }
        Collections.sort(lstInteger);
        return lstInteger;

    }
}
