/*
 * SedeDAOImpl.java
 *
 * Created on 23 de febrero de 2007, 08:52 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.dao;

import java.util.List;
import org.springside.core.dao.HibernateEntityDao;
import pre.model.dao.SedeDAOImpl;
import pre.model.vo.Sede;

/**
 *
 * @author jdgomez
 */
public class SedeDAOImpl extends HibernateEntityDao<Sede> { 

    public void saveSede(Sede sede) {
        save(sede);
    }
    
    public List<Sede> getSedes() {
        return getAll();
    }
    
}
