/*
 * MyExceptionVal.java
 *
 * Created on 15 de junio de 2004, 10:21
 */

package com.utils;

/**
 *
 * @author  jdgomez
 */
public class MyExceptionVal extends Exception {
    
    /** Creates a new instance of MyExceptionVal */
    public MyExceptionVal(String mess) {
        super(mess);
    }
    
}
