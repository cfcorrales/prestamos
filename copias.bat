@echo off
cd c:\
if not exist c:\Copia_Seguridad_Prestamos mkdir c:\Copia_Seguridad_Prestamos
set Any=%Date:~8%
set Mes=%Date:~3,2%
set Dia=%Date:~0,2%
set File=copiaprestamos%Dia%_%Mes%_%Any%
cd C:\Archivos de programa\MySQL\MySQL Server 4.1\bin
mysqldump --password=root --user=root --host=localhost prestamos > c:\Copia_Seguridad_Prestamos\%File%.sql
ping -n 5 127.0.0.1 > null
zip c:\Copia_Seguridad_Prestamos\%File%.zip c:\Copia_Seguridad_Prestamos\%File%.sql
del c:\Copia_Seguridad_Prestamos\%File%.sql