/*
 * CellEditorFecha.java
 *
 * Created on 7 de marzo de 2007, 08:26 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.general;

import java.awt.Component;
import java.util.Date;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author jdgomez
 */
public class CellEditorFecha extends AbstractCellEditor implements TableCellEditor {
    JXDatePicker comp = new JXDatePicker();
    
    /** Creates a new instance of CellEditorFecha */
    public CellEditorFecha() {
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        comp.setFont(new java.awt.Font("Tahoma", 0, 10));
        comp.setDate((Date)value);
        if (isSelected) {
            comp.setBackground(table.getSelectionBackground());
        }
        return comp;
    }

    @Override
    public Object getCellEditorValue() {
        return comp.getDate();
    }
    
}
