/*
 * Texto.java
 *
 * Created on 3 de marzo de 2004, 10:40 PM
 */

package com.utils;

import javax.swing.text.*;

/**
 *
 * @author  Admin
 */
public class Texto extends PlainDocument {
    public static int NORMAL_TYPE = 0;
    public static int INTEGER_TYPE = 1;
    public static int DOUBLE_TYPE = 2;
    public static int UPPER_TYPE = 3;
    public static int LOWER_TYPE = 4;
    public static int BIG_INTEGER =5;
    public static int TELEFONO_FAX =6;
    public static int CARACTERES = 7;    
    public static int DATE_TYPE = 8;
    public static int PORCENTAJE = 9;
    public static int FLOAT_TYPE = 10;
    private int maxCaracters = 0;
    private int type = 0;
    public String Validos = "";
    public String Invalidos = "";
    
    /** Creates a new instance of Texto */
    public Texto(int maxCaracters) {
        this(maxCaracters,NORMAL_TYPE);
    }
    
    public Texto(int maxCaracters, int type) {
        this(maxCaracters,type,"", "");
        
    }
    
    public Texto(int maxCaracters, int type,String Validos, String Invalidos){
        this.maxCaracters = maxCaracters;
        this.type = type;
        this.Validos = Validos;
        this.Invalidos = Invalidos;
    }
    
    private void validate(int offs, String str, javax.swing.text.AttributeSet a) throws javax.swing.text.BadLocationException {
        boolean band = true;
        try {
            switch(type){
                case 1: //INTEGER_TYPE
                    Integer.parseInt(getText(0,getLength())+str);
                    break;
                case 2: //DOUBLE_TYPE
                    Double.parseDouble(getText(0,getLength())+str);
                    break;
                case 3: //UPPER_TYPE
                    str = str.toUpperCase();
                    break;
                case 4: //LOWER_TYPE
                    str = str.toLowerCase();
                    break;
                case 5: //BIG_INTEGER
                    char[] car=new char[str.length()];
                    str.getChars(0,str.length(),car,0);
                    for(int b=0;b<str.length();b++){
                        if(!Character.isDigit(car[b]))
                            band=false;
                    }
                    break;
                case 6://TELEFONO_FAX
                    car=new char[str.length()];
                    str.getChars(0,str.length(),car,0);
                    for(int b=0;b<str.length();b++){
                        if(!Character.isDigit(car[b])&&!(car[b] =='(')&&!(car[b] ==')')&&!(car[b] =='-')&&!(car[b] ==' '))
                            band=false;
                    }
                    break;
                case 7://SOLO CARACTERES
                    car=new char[str.length()];
                    str.getChars(0,str.length(),car,0);
                    for(int b=0;b<str.length();b++){
                        if(!Character.isLetter(car[b])|| car[b]=='\'')
                            band=false;
                    }
                    break;
                case 8:// CARACTERES DE FECHAS
                    String temp = "";
                    for(int i =0; i < str.length(); i++){
                        if(isDateChar(str.charAt(i))){
                            temp = temp + str.charAt(i);
                        }
                    }
                    str = temp;                    
                    break;
                case 9: //PORCENTAJE
                    double d =Double.parseDouble(getText(0,getLength())+str);
                    band= (d>=0&&d<=100);                        
                    break;
                case 10: //FLOAT_TYPE
                    Float.parseFloat(getText(0,getLength())+str);
                    break;    
            }
            if(band&&validarValido(str)&&validarInvalido(str)){                
                super.insertString(offs, str, a);
            }
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }
    
    private boolean validarValido(String str){
        boolean resp = true;
        for(int a=0;a<str.length()&&Validos.length()!=0;a++){
            if(Validos.indexOf(str.charAt(a)+"")==-1)
                return false;
        }
        return resp;
    }
    private boolean validarInvalido(String str){
        boolean resp = true;
        for(int a=0;a<str.length()&&Invalidos.length()!=0;a++){
            if(Invalidos.indexOf(str.charAt(a)+"")!=-1)
                return false;
        }
        return resp;
    }
    
    private boolean isDateChar(char c){
        boolean resp = false;
        if(Character.isDigit(c) || c == '/' || c == '.' || c == '-')
            resp = true;
        return resp;
    }
    
    public void insertString(int offs, String str, javax.swing.text.AttributeSet a) throws javax.swing.text.BadLocationException {
        if(str == null) return;
        if(str.length() == 0) return;
        if(getLength()+str.length() <= maxCaracters)
            validate(offs, str, a);
    }
    
    public void remove(int offset, int len) throws javax.swing.text.BadLocationException {
        super.remove(offset, len);
    }
    
}