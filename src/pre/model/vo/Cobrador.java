/*
 * Cobrador.java
 *
 * Created on 23 de febrero de 2007, 09:46 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

import java.util.Date;

/**
 *
 * @author jdgomez
 */
public class Cobrador {
    /**
     * Holds value of property idCobrador.
     */
    private Integer idCobrador;
    private String usuario;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    
    /**
     * Getter for property idCobrador.
     * @return Value of property idCobrador.
     */
    public Integer getIdCobrador() {
        return this.idCobrador;
    }

    /**
     * Setter for property idCobrador.
     * @param idCobrador New value of property idCobrador.
     */
    public void setIdCobrador(Integer idCobrador) {
        this.idCobrador = idCobrador;
    }

    /**
     * Holds value of property persona.
     */
    private Persona persona;

    /**
     * Getter for property persona.
     * @return Value of property persona.
     */
    public Persona getPersona() {
        return this.persona;
    }

    /**
     * Setter for property persona.
     * @param persona New value of property persona.
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    /**
     * Holds value of property idSede.
     */
    private Integer idSede;

    /**
     * Getter for property idSede.
     * @return Value of property idSede.
     */
    public Integer getIdSede() {
        return this.idSede;
    }

    /**
     * Setter for property idSede.
     * @param idSede New value of property idSede.
     */
    public void setIdSede(Integer idSede) {
        this.idSede = idSede;
    }

    /**
     * Holds value of property zona.
     */
    private String zona;

    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public String getZona() {
        return this.zona;
    }

    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona(String zona) {
        this.zona = zona;
    }
    
    public String toString() {
        return persona.getNombre() + " " + persona.getApellidos();
    }

    /**
     * Holds value of property estado.
     */
    private Integer estado;

    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public Integer getEstado() {
        return this.estado;
    }

    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    
}
