/*
 * MyException.java
 *
 * Created on 15 de junio de 2004, 8:32
 */

package com.utils;

/**
 *
 * @author  jdgomez
 */
public class MyException extends Exception {
    
    /** Creates a new instance of MyException */
    public MyException(String mess) {
        super(mess);
    }
    
}
