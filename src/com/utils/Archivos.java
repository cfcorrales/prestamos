/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.utils;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import pre.cliente.paneles.PnlReportes;

/**
 *
 * @author Usuario UTP
 */
public class Archivos {
     public static void borrarArchivos(File directorio){
          File[] ficheros = directorio.listFiles();
           for (int x=0;x<ficheros.length;x++){
               ficheros[x].delete();
               }
    }
     public static void crearDirectorio(){
         String directorio = ResourceHelper.getProperty("prop/Propiedades.properties", "directorioTemp");
         java.io.File dir = new File(directorio);
         if(!dir.exists()){
              dir.mkdir();
              System.out.println("directorio no existe");
         }else
              System.out.println("directorio Si existe");

     }
      public static void abrirDocumento(String nombre,JasperPrint jp){
        try {
            String directorio = ResourceHelper.getProperty("prop/Propiedades.properties", "directorioTemp");
            JasperExportManager.exportReportToPdfFile(jp, directorio +"/"+ nombre);
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + directorio +"/"+ nombre);

        } catch (IOException e) {
             JOptionPane.showMessageDialog(null, "El reporte solicitado ya se encuentra abierto,\nsi desea recargar el documento cierre el que tiene abierto y vuelva a ver el reporte");
        } catch (JRException ex) {
                 Logger.getLogger(PnlReportes.class.getName()).log(Level.SEVERE, null,ex);
          }

       }
}
