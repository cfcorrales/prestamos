/*
 * CobradorDAOImpl.java
 *
 * Created on 30 de abril de 2007, 09:40 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.dao;

import java.util.List;
import org.springside.core.dao.HibernateEntityDao;
import pre.model.vo.Cobrador;

/**
 *
 * @author jdgomez
 */
public class CobradorDAOImpl extends HibernateEntityDao<Cobrador> {
    
    public void saveCobrador(Cobrador cobrador) {
        save(cobrador);
    }
    
    public List<Cobrador> findCobradorBySede(Integer idSede) {
        return findBy("idSede", idSede);
    }

    public Cobrador findCobradorByUsuario(String usuario){
        return findUniqueBy("usuario", usuario);
    }
    
}
