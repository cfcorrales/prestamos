/*
 * Codeudor.java
 *
 * Created on 23 de febrero de 2007, 09:58 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

/**
 *
 * @author jdgomez
 */
public class Codeudor {
    
    /**
     * Creates a new instance of Codeudor
     */
    public Codeudor() {
    }

    /**
     * Holds value of property idCodeudor.
     */
    private Long idCodeudor;

    /**
     * Getter for property idCodeudor.
     * @return Value of property idCodeudor.
     */
    public Long getIdCodeudor() {
        return this.idCodeudor;
    }

    /**
     * Setter for property idCodeudor.
     * @param idCodeudor New value of property idCodeudor.
     */
    public void setIdCodeudor(Long idCodeudor) {
        this.idCodeudor = idCodeudor;
    }

    /**
     * Holds value of property idCredito.
     */
    private Long idCredito;

    /**
     * Getter for property idCredito.
     * @return Value of property idCredito.
     */
    public Long getIdCredito() {
        return this.idCredito;
    }

    /**
     * Setter for property idCredito.
     * @param idCredito New value of property idCredito.
     */
    public void setIdCredito(Long idCredito) {
        this.idCredito = idCredito;
    }

    /**
     * Holds value of property documento.
     */
    private String documento;

    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public String getDocumento() {
        return this.documento;
    }

    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * Holds value of property nombre.
     */
    private String nombre;

    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Holds value of property direccionCasa.
     */
    private String direccionCasa;

    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public String getDireccionCasa() {
        return this.direccionCasa;
    }

    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccionCasa(String direccionCasa) {
        this.direccionCasa = direccionCasa;
    }

    /**
     * Holds value of property direccionTrabajo.
     */
    private String direccionTrabajo;

    /**
     * Getter for property dirTrabajo.
     * @return Value of property dirTrabajo.
     */
    public String getDireccionTrabajo() {
        return this.direccionTrabajo;
    }

    /**
     * Setter for property dirTrabajo.
     * @param dirTrabajo New value of property dirTrabajo.
     */
    public void setDireccionTrabajo(String direccionTrabajo) {
        this.direccionTrabajo = direccionTrabajo;
    }

    /**
     * Holds value of property ocupacion.
     */
    private String ocupacion;

    /**
     * Holds value of property telefonoCasa.
     */
    private String telefonoCasa;

    /**
     * Getter for property telefono.
     * @return Value of property telefono.
     */
    public String getTelefonoCasa() {
        return this.telefonoCasa;
    }

    /**
     * Setter for property telefono.
     * @param telefono New value of property telefono.
     */
    public void setTelefonoCasa(String telefonoCasa) {
        this.telefonoCasa = telefonoCasa;
    }

    /**
     * Holds value of property celular.
     */
    private String celular;

    /**
     * Getter for property celular.
     * @return Value of property celular.
     */
    public String getCelular() {
        return this.celular;
    }

    /**
     * Setter for property celular.
     * @param celular New value of property celular.
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * Holds value of property telefonoTrabajo.
     */
    private String telefonoTrabajo;

    /**
     * Getter for property telefonoTrabajo.
     * @return Value of property telefonoTrabajo.
     */
    public String getTelefonoTrabajo() {
        return this.telefonoTrabajo;
    }

    /**
     * Setter for property telefonoTrabajo.
     * @param telefonoTrabajo New value of property telefonoTrabajo.
     */
    public void setTelefonoTrabajo(String telefonoTrabajo) {
        this.telefonoTrabajo = telefonoTrabajo;
    }

    /**
     * Holds value of property numeroCodeudor.
     */
    private Integer numeroCodeudor;

    /**
     * Getter for property numeroCodeudor.
     * @return Value of property numeroCodeudor.
     */
    public Integer getNumeroCodeudor() {
        return this.numeroCodeudor;
    }

    /**
     * Setter for property numeroCodeudor.
     * @param numeroCodeudor New value of property numeroCodeudor.
     */
    public void setNumeroCodeudor(Integer numeroCodeudor) {
        this.numeroCodeudor = numeroCodeudor;
    }
    
}
