/*
 * PersonaDAOImpl.java
 *
 * Created on 20 de abril de 2007, 04:07 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.dao;

import java.util.List;
import org.springside.core.dao.HibernateEntityDao;
import pre.model.vo.Persona;

/**
 *
 * @author jdgomez
 */
public class PersonaDAOImpl extends HibernateEntityDao<Persona> {
    
    public void savePersona(Persona persona) {
        save(persona);
    }
    
    public Persona findPersonaById(Long idPersona) {
        return findUniqueBy("idPersona", idPersona);
    }
    
    public Persona findPersonaByDocumento(String numeroDocumento) {
        return findUniqueBy("documento", numeroDocumento);
    }
    
    public List<Persona> getAllPersonas() {
        return find("from Persona order by nombre");
    }
}
