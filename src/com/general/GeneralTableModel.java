/*
 * TableModel.java
 *
 * Created on 2 de marzo de 2007, 09:01 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.general;

import com.utils.Utilidades;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.swing.table.AbstractTableModel;





/**
 *
 * @author jdgomez
 */
public class GeneralTableModel extends AbstractTableModel {
    private static final Class[] EMPTY_PARAMETER_TYPES = new Class[0];
    private List datos;
    private String[] propiedades;
    private String[] titulos;
    private Class[] clases;
    private Boolean[] editable;
    
  
    public GeneralTableModel(List datos, String[] propiedades, String[] titulos) {
        this.datos = datos;
        this.propiedades = propiedades;
        this.titulos = titulos;
        clases = null;
        editable = null;
    }
    
    public GeneralTableModel(List datos, String[] propiedades, String[] titulos, Class[] clases) {
        this.datos = datos;
        this.propiedades = propiedades;
        this.titulos = titulos;
        this.clases = clases;
        editable = null;
    }
    
    public GeneralTableModel(List datos, String[] propiedades, String[] titulos, Class[] clases, Boolean[] editable) {
        this.datos = datos;
        this.propiedades = propiedades;
        this.titulos = titulos;
        this.clases = clases;
        this.editable = editable;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            String metodo = "get" + Utilidades.capitalize(propiedades[columnIndex]);
            
            return  datos.get(rowIndex).getClass().getMethod(metodo, EMPTY_PARAMETER_TYPES).invoke(datos.get(rowIndex));
        } catch (IllegalArgumentException ex) {
            System.out.println("error: "+ex.getMessage());
        } catch (SecurityException ex) {
            System.out.println("error: "+ex.getMessage());
        } catch (NoSuchMethodException ex) {
            System.out.println("error: "+ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println("error: "+ex.getMessage());
        } catch (InvocationTargetException ex) {
            System.out.println("error: "+ex.getMessage());
        }
        return null;
    }

    @Override
    public int getRowCount() {
        return datos!=null?datos.size():0;
    }

    @Override
    public int getColumnCount() {
        return titulos.length;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return titulos[columnIndex];
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (editable == null)
            return true;
        else
            return editable[columnIndex];
    }
    
    @Override
    public Class getColumnClass(int columnIndex) {        
        if (clases == null)
            return Class.class;
        return clases[columnIndex];
    }
/*
    private List datos;
    private Expression[] ognlExprs;
    private String[] propiedades;
    private String[] titulos;
    private Class[] clases;
    private Boolean[] editable;
    private Map<Integer, Map> columnas = new HashMap<Integer, Map>();

  
    public GeneralTableModel(List datos, String[] propiedades, String[] titulos) {
        this(datos, propiedades, titulos, null, null);
    }

    public GeneralTableModel(List datos, String[] propiedades, String[] titulos, Class[] clases) {
        this(datos, propiedades, titulos, clases, null);
    }

    public GeneralTableModel(List datos, String[] propiedades, String[] titulos, Class[] clases, Boolean[] editable) {
        this.datos = datos;
        this.propiedades = propiedades;
        this.titulos = titulos;
        this.clases = clases;
        this.editable = editable;
        ognlExprs = new Expression[propiedades.length];
        for (int i = 0; i < propiedades.length; i++) {
            try {
                String columnExpr = propiedades[i];
                ognlExprs[i] = Ognl.getInstancia().getExpression(columnExpr);
            } catch (ExpressionSyntaxException e) {
                System.out.println(e.getMessage());
            } catch (OgnlException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            Object value = Ognl.getInstancia().findValue(ognlExprs[columnIndex], datos.get(rowIndex));
            if (null != columnas.get(columnIndex) && null != value)
                value = columnas.get(columnIndex).get(value);
            return value;
        } catch (OgnlException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public int getRowCount() {
        return datos!=null?datos.size():0;
    }

    public int getColumnCount() {
        return titulos.length;
    }

    public String getColumnName(int columnIndex) {
        return titulos[columnIndex];
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (editable == null)
            return false;
        return editable[columnIndex];
    }

    public Class getColumnClass(int columnIndex) {
        if (clases == null)
            return Class.class;
        return clases[columnIndex];
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        try {
            Ognl.getInstancia().setValue(propiedades[columnIndex], datos.get(rowIndex), value);
            fireTableCellUpdated(rowIndex, columnIndex);
        } catch (OgnlException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }*/
        /*String metodo = "set" + Utilidades.capitalize(propiedades[columnIndex]);
        try{
            datos.get(rowIndex).getClass().getMethod(metodo, new Class[] {clases[columnIndex]}).invoke(datos.get(rowIndex), new Object[] {value});
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }*/
    }
/*
    public void setDatos(List datos){
        this.datos=datos;
        super.fireTableDataChanged();
    }

    public List getItems() {
        return datos;
    }

    public void addRow(Object row, int index){
        datos.add(index,row);
        super.fireTableRowsInserted(index,index);
        super.fireTableDataChanged();
    }

    public void removeRow(int index){
        datos.remove(index);
        super.fireTableRowsDeleted(index,index);
        super.fireTableDataChanged();
    }

    public Object getRow(int row){
        return datos.get(row);
    }

    public void setColumnRenderer(int columnIndex, Map columnMap) {
        columnas.put(columnIndex, columnMap);
    }

    public Object getItemAt(int row){
        return (row < datos.size() ? datos.get(row) : null);
    }


*/

