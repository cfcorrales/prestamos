/*
 * PagosTableModel.java
 *
 * Created on 10 de marzo de 2007, 08:33 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.general;

import java.util.List;
import pre.model.vo.Pago;

/**
 *
 * @author jdgomez
 */
public class PagosTableModel extends GeneralTableModel {
    private List<Pago> datos;
    
    /** Creates a new instance of PagosTableModel */
    public PagosTableModel(List datos, String[] propiedades, String[] titulos, Class[] clases) {
        super(datos, propiedades, titulos, clases);
        this.datos = datos;
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
       /* if (columnIndex == 2 || columnIndex == 3)
             return true;        
        else*/
            return false;
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      /* Pago pago = (Pago)datos.get(rowIndex);
        switch(columnIndex) {
            case 1:
                pago.setFechaPago((Date)aValue);
                if(null != aValue)
                    pago.setFechaIngresoPago(new Date());
                break;
            case 2:
                pago.setValorPagado((Float)aValue);
                break;
        }*/
    }

    @Override
    public int getColumnCount() {
        return super.getColumnCount();
    }

    @Override
    public int getRowCount() {
        return super.getRowCount();
    }

}
