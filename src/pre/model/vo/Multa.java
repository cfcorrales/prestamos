/*
 * Multa.java
 *
 * Created on 23 de febrero de 2007, 10:04 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pre.model.vo;

import java.util.Date;

/**
 *
 * @author jdgomez
 */
public class Multa {
    /**
     * Holds value of property idMulta.
     */
    private Long idMulta;

    /**
     * Getter for property idMulta.
     * @return Value of property idMulta.
     */
    public Long getIdMulta() {
        return this.idMulta;
    }

    /**
     * Setter for property idMulta.
     * @param idMulta New value of property idMulta.
     */
    public void setIdMulta(Long idMulta) {
        this.idMulta = idMulta;
    }

    /**
     * Holds value of property idCredito.
     */
    private Long idCredito;

    /**
     * Getter for property idCredito.
     * @return Value of property idCredito.
     */
    public Long getIdCredito() {
        return this.idCredito;
    }

    /**
     * Setter for property idCredito.
     * @param idCredito New value of property idCredito.
     */
    public void setIdCredito(Long idCredito) {
        this.idCredito = idCredito;
    }

    /**
     * Holds value of property valorMulta.
     */
    private Float valorMulta;

    /**
     * Getter for property valorMulta.
     * @return Value of property valorMulta.
     */
    public Float getValorMulta() {
        return this.valorMulta;
    }

    /**
     * Setter for property valorMulta.
     * @param valorMulta New value of property valorMulta.
     */
    public void setValorMulta(Float valorMulta) {
        this.valorMulta = valorMulta;
    }

    /**
     * Holds value of property fechaMulta.
     */
    private Date fechaMulta;

    /**
     * Getter for property fechaMulta.
     * @return Value of property fechaMulta.
     */
    public Date getFechaMulta() {
        return this.fechaMulta;
    }

    /**
     * Setter for property fechaMulta.
     * @param fechaMulta New value of property fechaMulta.
     */
    public void setFechaMulta(Date fechaMulta) {
        this.fechaMulta = fechaMulta;
    }

    /**
     * Holds value of property numPago.
     */
    private Integer numPago;

    /**
     * Getter for property numPago.
     * @return Value of property numPago.
     */
    public Integer getNumPago() {
        return this.numPago;
    }

    /**
     * Setter for property numPago.
     * @param numPago New value of property numPago.
     */
    public void setNumPago(Integer numPago) {
        this.numPago = numPago;
    }

    /**
     * Holds value of property estado.
     */
    private Integer estado;

    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public Integer getEstado() {
        return this.estado;
    }

    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(Integer estado) {
        this.estado = estado;
    }
    
}
