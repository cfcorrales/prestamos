/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general;

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author Usuario UTP
 */
public class CellEditorText extends AbstractCellEditor implements TableCellEditor{
    JTextField comp = new JTextField();

    /** Creates a new instance of CellEditorFecha */


    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if(null!=value){
         //   comp.setFont(new java.awt.Font("Tahoma", 0, 10));
            if(null!=value)
               comp.setText((String)value.toString());
        }
        if(isSelected) {
          comp.setBackground(table.getSelectionBackground());
        }
        return comp;
    }

    @Override
    public Object getCellEditorValue() {
        return comp.getText();
    }
}
